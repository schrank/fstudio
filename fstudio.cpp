#include "test/FEMNewmarkSolverTest.h"
#include "test/FEMPropertiesTest.h"

int main()
{
  //{
  //  tests::FEMPropertiesTest* fpt = new tests::FEMPropertiesTest;
  //  fpt->Run();
  //  delete fpt;
  //}

  {
    tests::FEMNewmarkSolverTest* fnst = new tests::FEMNewmarkSolverTest;
    fnst->Run();
    delete fnst;
  }

  return 0;
}