#pragma once

#include "Test.h"

namespace tests
{

  class FEMPropertiesTest:
    virtual public Test
  {
  public:
    FEMPropertiesTest();
    virtual ~FEMPropertiesTest();

    bool Run() override;
  };

}