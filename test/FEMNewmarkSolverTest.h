#pragma once

#include "Test.h"

namespace tests
{

  class FEMNewmarkSolverTest:
    virtual public Test
  {
  public:
    FEMNewmarkSolverTest();
    virtual ~FEMNewmarkSolverTest();

    bool Run() override;
  };

}