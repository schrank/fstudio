#include "Test.h"

namespace tests
{

  Test::Test() : _name("Test Base Class") {}

  Test::Test(std::string name): _name(name) {}

  Test::~Test() {}

  std::string Test::GetName() const
  {
    return _name;
  }

}