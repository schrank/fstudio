#include "FEMNewmarkSolverTest.h"

#include <iostream>
#include <stdexcept>

#include "../fem/properties/Density.h"
#include "../mesh/generator/MeshGenerator.h"

#include "../fem/analysis/AnalysisModel.h"
#include "../fem/analysis/DynamicAnalysis.h"
#include "../fem/analysis/IntegratorDynamic.h"
#include "../fem/analysis/LinearSOE.h"

#include "../fem/solver/NewmarkSolver.h"

#include "../fem/mesh/FEMesh.h"

#include "../linearSolver/DPCGSolver.h"

namespace tests
{
  FEMNewmarkSolverTest::FEMNewmarkSolverTest() : Test("FEM Newmark Solver Test") {}

  FEMNewmarkSolverTest::~FEMNewmarkSolverTest() {}

	using namespace fem;

  bool FEMNewmarkSolverTest::Run()
  {
		try
		{
			std::shared_ptr<femesh::FEMesh> femesh = std::make_shared<femesh::FEMesh>();
			{
        mesh::generator::MeshGenerator* meshGenerator = new mesh::generator::MeshGenerator;

        meshGenerator->CreateBlock(femesh, 1, 1, 1, 2, 2, 2);

        for (mesh::PrimitivePtr cell : femesh->GetCells())
        {
          std::shared_ptr<fem::prop::Density> density = std::make_shared<fem::prop::Density>(100);

          cell->AddProperty(density);
        }
			}

			std::shared_ptr<analysis::IntegratorDynamic> integrator = std::make_shared<analysis::IntegratorDynamic>();
      std::shared_ptr<analysis::LinearSOE> soe = std::make_shared<analysis::LinearSOE>();

			std::shared_ptr<ls::DPCGSolver> linSolver = std::make_shared<ls::DPCGSolver>();

			std::shared_ptr<solver::NewmarkSolver> soeSolver =
				std::make_shared<solver::NewmarkSolver>(linSolver, 1e-6);

			std::shared_ptr<analysis::AnalysisModel> model =
				std::make_shared<analysis::AnalysisModel>(femesh, integrator, soe, soeSolver);

			std::shared_ptr<analysis::DynamicAnalysis> analysis = std::make_shared<analysis::DynamicAnalysis>(model);

			analysis->Analyze();
		}
		catch (std::logic_error& e)
		{
			std::cout << std::endl << e.what() << std::endl;
		}
		catch (std::bad_alloc& e)
		{
			std::cout << std::endl << "bad memory allocation!" << std::endl;
		}

		std::cout << "End the program..." << std::endl;

		return true;
  }
}