#include "FEMPropertiesTest.h"

#include "../fem/mesh/FEMesh.h"
#include "../fem/exports/VTKFEMManager.h"
#include "../fem/properties/Density.h"
#include "../mesh/generator/MeshGenerator.h"
#include "../mesh/mesh/Primitive.h"

namespace tests
{
  FEMPropertiesTest::FEMPropertiesTest(): Test("FEM Mesh Properties Test") {}

  FEMPropertiesTest::~FEMPropertiesTest() {}

  bool FEMPropertiesTest::Run()
  {
    mesh::generator::MeshGenerator* meshGenerator = new mesh::generator::MeshGenerator;

    std::shared_ptr<fem::femesh::FEMesh> mesh = std::make_shared<fem::femesh::FEMesh>();
    meshGenerator->CreateBlock(mesh, 1, 1, 1, 2, 2, 2);

    for (mesh::PrimitivePtr cell : mesh->GetCells())
    {
      std::shared_ptr<fem::prop::Density> density = std::make_shared<fem::prop::Density>(100);

      cell->AddProperty(density);
    }

    fem::exports::VTKFEMManager* exportManager = new fem::exports::VTKFEMManager;
    exportManager->Export(mesh, "data.vtu");

    delete meshGenerator;
    delete exportManager;

    return true;
  }
}