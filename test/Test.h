#pragma once

#include <string>

namespace tests
{

  class Test
  {
  public:
    Test();
    Test(std::string name);
    virtual ~Test();

    std::string GetName() const;

    virtual bool Run() = 0;

  protected:
    std::string _name;
  };

}