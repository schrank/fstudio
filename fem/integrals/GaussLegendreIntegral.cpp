#include "GaussLegendreIntegral.h"

#include <stdexcept>

namespace fem
{
	namespace integrals
	{

		double GaussLegendreIntegral::local[27][3] =
		{
			//	local node: 1
			{-1, -1, -1},
			//	local node: 2
			{-1, 1, -1},
			//	local node: 3
			{-1, 1, 1},
			//	local node: 4
			{-1, -1, 1},
			//	local node: 5
			{1, -1, -1},
			//	local node: 6
			{1, -1, 1},
			//	local node: 7
			{1, 1, 1},
			//	local node: 8
			{1, 1, -1},
			//...................................................................
			//	local edge: 1
			{-1, 0, -1},
			//	local edge: 2
			{-1, 1, 0},
			//	local edge: 3
			{-1, 0, 1},
			//	local edge: 4
			{-1, -1, 0},
			//	local edge: 5
			{0, -1, -1},
			//	local edge: 6
			{0, 1, -1},
			//	local edge: 7
			{0, 1, 1},
			//	local edge: 8
			{0, -1, 1},
			//	local edge: 9
			{1, -1, 0},
			//	local edge: 10
			{1, 0, 1},
			//	local edge: 11
			{1, 1, 0},
			//	local edge: 12
			{1, 0, -1},
			//...................................................................
			//	local face: 1
			{-1, 0, 0},
			//	local face: 2
			{0, 0, -1},
			//	local face: 3
			{0, 1, 0},
			//	local face: 4
			{0, 0, 1},
			//	local face: 5
			{0, -1, 0},
			//	local face: 6
			{1, 0, 0},
			//...................................................................
			//	local inner element: 1
			{0, 0, 0}
		};

		GaussLegendreIntegral::GaussLegendreIntegral() : Integral(), _gaussPoints({}) {}
		GaussLegendreIntegral::~GaussLegendreIntegral() {}

		void GaussLegendreIntegral::setNumberGaussPoints(unsigned int num)
		{
			if (num > 0 && num <= 16)
			{
				_gaussPoints.resize(num);

				createGaussPoints();
			}
			else
			{
				throw std::logic_error("GaussLegendreIntegral::set_NumberGaussPoints: Invalid Number Of Gauss Points");
			}
		}

		unsigned int GaussLegendreIntegral::GetNumberGaussPoints()
		{
			return _gaussPoints.size();
		}

		std::vector<GaussPoint>& GaussLegendreIntegral::GetGaussPoints()
		{
			return _gaussPoints;
		}

		void GaussLegendreIntegral::createGaussPoints()
		{
			unsigned int numberGaussPoints = GetNumberGaussPoints();

			if (numberGaussPoints > 0 && numberGaussPoints <= 16)
			{
				switch (numberGaussPoints)
				{
				case 1:
					_gaussPoints[0]._coord = 0.;
					_gaussPoints[0]._length = 2.;
					break;
				case 2:
					_gaussPoints[0]._coord = .577350269189626;
					_gaussPoints[1]._coord = -.577350269189626;
					_gaussPoints[0]._length = 1.;
					_gaussPoints[1]._length = 1.;
					break;
				case 3:
					_gaussPoints[0]._coord = 0.;
					_gaussPoints[1]._coord = .774596669241483;
					_gaussPoints[2]._coord = -.774596669241483;
					_gaussPoints[0]._length = .888888888888889;
					_gaussPoints[1]._length = .555555555555556;
					_gaussPoints[2]._length = .555555555555556;
					break;
				case 4:
					_gaussPoints[0]._coord = .339981043584856;
					_gaussPoints[1]._coord = -.339981043584856;
					_gaussPoints[2]._coord = .861136311594053;
					_gaussPoints[3]._coord = -.861136311594053;
					_gaussPoints[0]._length = .652145154862546;
					_gaussPoints[1]._length = .652145154862546;
					_gaussPoints[2]._length = .347854845137454;
					_gaussPoints[3]._length = .347854845137454;
					break;
				case 5:
					_gaussPoints[0]._coord = 0.;
					_gaussPoints[1]._coord = .538469310105683;
					_gaussPoints[2]._coord = -.538469310105683;
					_gaussPoints[3]._coord = .906179845938664;
					_gaussPoints[4]._coord = -.906179845938664;
					_gaussPoints[0]._length = .568888888888889;
					_gaussPoints[1]._length = .478628670499366;
					_gaussPoints[2]._length = .478628670499366;
					_gaussPoints[3]._length = .236926885056189;
					_gaussPoints[4]._length = .236926885056189;
					break;
				case 6:
					_gaussPoints[0]._coord = -.238619186083196;
					_gaussPoints[1]._coord = .238619186083196;
					_gaussPoints[2]._coord = -.661209386466265;
					_gaussPoints[3]._coord = .661209386466265;
					_gaussPoints[4]._coord = -.932469514203152;
					_gaussPoints[5]._coord = .932469514203152;
					_gaussPoints[0]._length = .467913934572691;
					_gaussPoints[1]._length = .467913934572691;
					_gaussPoints[2]._length = .360761573048139;
					_gaussPoints[3]._length = .360761573048139;
					_gaussPoints[4]._length = .171324492379170;
					_gaussPoints[5]._length = .171324492379170;
					break;
				case 7:
					_gaussPoints[0]._coord = 0.;
					_gaussPoints[1]._coord = .405845151377397;
					_gaussPoints[2]._coord = -.405845151377397;
					_gaussPoints[3]._coord = .741531185599394;
					_gaussPoints[4]._coord = -.741531185599394;
					_gaussPoints[5]._coord = .949107912342758;
					_gaussPoints[6]._coord = -.949107912342758;
					_gaussPoints[0]._length = .417959183673469;
					_gaussPoints[1]._length = .381830050505119;
					_gaussPoints[2]._length = .381830050505119;
					_gaussPoints[3]._length = .279705391489277;
					_gaussPoints[4]._length = .279705391489277;
					_gaussPoints[5]._length = .129484966168870;
					_gaussPoints[6]._length = .129484966168870;
					break;
				case 8:
					_gaussPoints[0]._coord = -.183434642495650;
					_gaussPoints[1]._coord = .183434642495650;
					_gaussPoints[2]._coord = -.525532409916329;
					_gaussPoints[3]._coord = .525532409916329;
					_gaussPoints[4]._coord = -.796666477413627;
					_gaussPoints[5]._coord = .796666477413627;
					_gaussPoints[6]._coord = -.960289856497536;
					_gaussPoints[7]._coord = .960289856497536;
					_gaussPoints[0]._length = .362683783378362;
					_gaussPoints[1]._length = .362683783378362;
					_gaussPoints[2]._length = .313706645877887;
					_gaussPoints[3]._length = .313706645877887;
					_gaussPoints[4]._length = .222381034453374;
					_gaussPoints[5]._length = .222381034453374;
					_gaussPoints[6]._length = .101228536290376;
					_gaussPoints[7]._length = .101228536290376;
					break;
				case 9:
					_gaussPoints[0]._coord = 0.;
					_gaussPoints[1]._coord = .324253423403809;
					_gaussPoints[2]._coord = -.324253423403809;
					_gaussPoints[3]._coord = .613371432700590;
					_gaussPoints[4]._coord = -.613371432700590;
					_gaussPoints[5]._coord = .836031107326636;
					_gaussPoints[6]._coord = -.836031107326636;
					_gaussPoints[7]._coord = .968160239507626;
					_gaussPoints[8]._coord = -.968160239507626;
					_gaussPoints[0]._length = .330239355001260;
					_gaussPoints[1]._length = .312347077040003;
					_gaussPoints[2]._length = .312347077040003;
					_gaussPoints[3]._length = .260610696402935;
					_gaussPoints[4]._length = .260610696402935;
					_gaussPoints[5]._length = .180648160694857;
					_gaussPoints[6]._length = .180648160694857;
					_gaussPoints[7]._length = .081274388361574;
					_gaussPoints[8]._length = .081274388361574;
					break;
				case 10:
					_gaussPoints[0]._coord = -.148874338981631;
					_gaussPoints[1]._coord = .148874338981631;
					_gaussPoints[2]._coord = -.433395394129247;
					_gaussPoints[3]._coord = .433395394129247;
					_gaussPoints[4]._coord = -.679409568299024;
					_gaussPoints[5]._coord = .679409568299024;
					_gaussPoints[6]._coord = -.865063366688985;
					_gaussPoints[7]._coord = .865063366688985;
					_gaussPoints[8]._coord = -.973906528517172;
					_gaussPoints[9]._coord = .973906528517172;
					_gaussPoints[0]._length = .295524224714753;
					_gaussPoints[1]._length = .295524224714753;
					_gaussPoints[2]._length = .269266719309996;
					_gaussPoints[3]._length = .269266719309996;
					_gaussPoints[4]._length = .219086362515982;
					_gaussPoints[5]._length = .219086362515982;
					_gaussPoints[6]._length = .149451349150581;
					_gaussPoints[7]._length = .149451349150581;
					_gaussPoints[8]._length = .066671344308688;
					_gaussPoints[9]._length = .066671344308688;
					break;
				case 11:
					_gaussPoints[0]._coord = 0.;
					_gaussPoints[1]._coord = .269543155952345;
					_gaussPoints[2]._coord = -.269543155952345;
					_gaussPoints[3]._coord = .519096129206812;
					_gaussPoints[4]._coord = -.519096129206812;
					_gaussPoints[5]._coord = .730152005574049;
					_gaussPoints[6]._coord = -.730152005574049;
					_gaussPoints[7]._coord = .887062599768095;
					_gaussPoints[8]._coord = -.887062599768095;
					_gaussPoints[9]._coord = .978228659146057;
					_gaussPoints[10]._coord = -.978228659146057;
					_gaussPoints[0]._length = .272925086777901;
					_gaussPoints[1]._length = .262804544510247;
					_gaussPoints[2]._length = .262804544510247;
					_gaussPoints[3]._length = .233193764591990;
					_gaussPoints[4]._length = .233193764591990;
					_gaussPoints[5]._length = .186290210927734;
					_gaussPoints[6]._length = .186290210927734;
					_gaussPoints[7]._length = .125580369464905;
					_gaussPoints[8]._length = .125580369464905;
					_gaussPoints[9]._length = .055668567116174;
					_gaussPoints[10]._length = .055668567116174;
					break;
				case 12:
					_gaussPoints[0]._coord = -.125233408511468;
					_gaussPoints[1]._coord = .125233408511468;
					_gaussPoints[2]._coord = -.367831498998180;
					_gaussPoints[3]._coord = .367831498998180;
					_gaussPoints[4]._coord = -.587317954286617;
					_gaussPoints[5]._coord = .587317954286617;
					_gaussPoints[6]._coord = -.769902674194304;
					_gaussPoints[7]._coord = .769902674194304;
					_gaussPoints[8]._coord = -.904117256370474;
					_gaussPoints[9]._coord = .904117256370474;
					_gaussPoints[10]._coord = -.981560634246719;
					_gaussPoints[11]._coord = .981560634246719;
					_gaussPoints[0]._length = .249147045813402;
					_gaussPoints[1]._length = .249147045813402;
					_gaussPoints[2]._length = .233492536538354;
					_gaussPoints[3]._length = .233492536538354;
					_gaussPoints[4]._length = .203167426723065;
					_gaussPoints[5]._length = .203167426723065;
					_gaussPoints[6]._length = .160078328543346;
					_gaussPoints[7]._length = .160078328543346;
					_gaussPoints[8]._length = .106939325995318;
					_gaussPoints[9]._length = .106939325995318;
					_gaussPoints[10]._length = .047175336386511;
					_gaussPoints[11]._length = .047175336386511;
					break;
				case 13:
					_gaussPoints[0]._coord = 0.;
					_gaussPoints[1]._coord = .230458315955134;
					_gaussPoints[2]._coord = -.230458315955134;
					_gaussPoints[3]._coord = .448492751036446;
					_gaussPoints[4]._coord = -.448492751036446;
					_gaussPoints[5]._coord = .642349339440340;
					_gaussPoints[6]._coord = -.642349339440340;
					_gaussPoints[7]._coord = .801578090733309;
					_gaussPoints[8]._coord = -.801578090733309;
					_gaussPoints[9]._coord = .917598399222977;
					_gaussPoints[10]._coord = -.917598399222977;
					_gaussPoints[11]._coord = .984183054718588;
					_gaussPoints[12]._coord = -.984183054718588;
					_gaussPoints[0]._length = .232551553230873;
					_gaussPoints[1]._length = .226283180262897;
					_gaussPoints[2]._length = .226283180262897;
					_gaussPoints[3]._length = .207816047536888;
					_gaussPoints[4]._length = .207816047536888;
					_gaussPoints[5]._length = .178145980761945;
					_gaussPoints[6]._length = .178145980761945;
					_gaussPoints[7]._length = .138873510219787;
					_gaussPoints[8]._length = .138873510219787;
					_gaussPoints[9]._length = .092121499837728;
					_gaussPoints[10]._length = .092121499837728;
					_gaussPoints[11]._length = .040484004765315;
					_gaussPoints[12]._length = .040484004765315;
					break;
				case 14:
					_gaussPoints[0]._coord = -.108054948707343;
					_gaussPoints[1]._coord = .108054948707343;
					_gaussPoints[2]._coord = -.319112368927889;
					_gaussPoints[3]._coord = .319112368927889;
					_gaussPoints[4]._coord = -.515248636358154;
					_gaussPoints[5]._coord = .515248636358154;
					_gaussPoints[6]._coord = -.687292904811685;
					_gaussPoints[7]._coord = .687292904811685;
					_gaussPoints[8]._coord = -.827201315069764;
					_gaussPoints[9]._coord = .827201315069764;
					_gaussPoints[10]._coord = -.928434883663573;
					_gaussPoints[11]._coord = .928434883663573;
					_gaussPoints[12]._coord = -.986283808696812;
					_gaussPoints[13]._coord = .98628380869681;
					_gaussPoints[0]._length = .215263853463157;
					_gaussPoints[1]._length = .215263853463157;
					_gaussPoints[2]._length = .205198463721295;
					_gaussPoints[3]._length = .205198463721295;
					_gaussPoints[4]._length = .185538397477937;
					_gaussPoints[5]._length = .185538397477937;
					_gaussPoints[6]._length = .157203167158193;
					_gaussPoints[7]._length = .157203167158193;
					_gaussPoints[8]._length = .121518570687903;
					_gaussPoints[9]._length = .121518570687903;
					_gaussPoints[10]._length = .080158087159760;
					_gaussPoints[11]._length = .080158087159760;
					_gaussPoints[12]._length = .035119460331751;
					_gaussPoints[13]._length = .035119460331751;
					break;
				case 15:
					_gaussPoints[0]._coord = 0.;
					_gaussPoints[1]._coord = .201194093997434;
					_gaussPoints[2]._coord = -.201194093997434;
					_gaussPoints[3]._coord = .394151347077563;
					_gaussPoints[4]._coord = -.394151347077563;
					_gaussPoints[5]._coord = .570972172608538;
					_gaussPoints[6]._coord = -.570972172608538;
					_gaussPoints[7]._coord = .724417731360170;
					_gaussPoints[8]._coord = -.724417731360170;
					_gaussPoints[9]._coord = .848206583410427;
					_gaussPoints[10]._coord = -.848206583410427;
					_gaussPoints[11]._coord = .937273392400705;
					_gaussPoints[12]._coord = -.937273392400705;
					_gaussPoints[13]._coord = -.987992518020485;
					_gaussPoints[14]._coord = .987992518020485;
					_gaussPoints[0]._length = .202578241925561;
					_gaussPoints[1]._length = .198431485327111;
					_gaussPoints[2]._length = .198431485327111;
					_gaussPoints[3]._length = .186161000015562;
					_gaussPoints[4]._length = .186161000015562;
					_gaussPoints[5]._length = .166269205816993;
					_gaussPoints[6]._length = .166269205816993;
					_gaussPoints[7]._length = .139570677926154;
					_gaussPoints[8]._length = .139570677926154;
					_gaussPoints[9]._length = .107159220467171;
					_gaussPoints[10]._length = .107159220467171;
					_gaussPoints[11]._length = .070366047488108;
					_gaussPoints[12]._length = .070366047488108;
					_gaussPoints[13]._length = .030753241996117;
					_gaussPoints[14]._length = .030753241996117;
					break;
				case 16:
					_gaussPoints[0]._coord = -.095012509837637;
					_gaussPoints[1]._coord = .095012509837637;
					_gaussPoints[2]._coord = -.281603550779258;
					_gaussPoints[3]._coord = .281603550779258;
					_gaussPoints[4]._coord = -.458016777657227;
					_gaussPoints[5]._coord = .458016777657227;
					_gaussPoints[6]._coord = -.617876244402643;
					_gaussPoints[7]._coord = .617876244402643;
					_gaussPoints[8]._coord = -.755404408355003;
					_gaussPoints[9]._coord = .755404408355003;
					_gaussPoints[10]._coord = -.865631202387831;
					_gaussPoints[11]._coord = .865631202387831;
					_gaussPoints[12]._coord = -.944575023073232;
					_gaussPoints[13]._coord = .944575023073232;
					_gaussPoints[14]._coord = -.989400934991649;
					_gaussPoints[15]._coord = .989400934991649;
					_gaussPoints[0]._length = .189450610455068;
					_gaussPoints[1]._length = .189450610455068;
					_gaussPoints[2]._length = .182603415044923;
					_gaussPoints[3]._length = .182603415044923;
					_gaussPoints[4]._length = .169156519395002;
					_gaussPoints[5]._length = .169156519395002;
					_gaussPoints[6]._length = .149595988816576;
					_gaussPoints[7]._length = .149595988816576;
					_gaussPoints[8]._length = .124628971255533;
					_gaussPoints[9]._length = .124628971255533;
					_gaussPoints[10]._length = .095158511682492;
					_gaussPoints[11]._length = .095158511682492;
					_gaussPoints[12]._length = .062253523938647;
					_gaussPoints[13]._length = .062253523938647;
					_gaussPoints[14]._length = .027152459411754;
					_gaussPoints[15]._length = .027152459411754;
				}
			}
			else
			{
				throw std::logic_error("GaussLegendreIntegral::createGaussPoints: Invalid Number Of Gauss Points");
			}
		}

	}
}