#pragma once

#include "Function.h"

namespace fem
{

  namespace integrals
  {

    class Function1D:
      virtual public Function
    {
    public:
      Function1D(void) {};

      virtual ~Function1D(void) {};

      virtual double F(double x) = 0;
    };

  }

}