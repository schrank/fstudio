#include "HierarchicalShapeFunction.h"

namespace fem
{

  namespace integrals
  {

    HierarchicalShapeFunction::HierarchicalShapeFunction() {}

    HierarchicalShapeFunction::HierarchicalShapeFunction(
      int xp, int yp, int zp, double x, double y, double z)
    {
      if (xp == 1)
      {
        _X.SetDegree(1);
        _X.SetFactor(0, 0.5);
        _X.SetFactor(1, x / 2);
      }
      else
        _X = _generator.ShapeFunction(xp);

      if (yp == 1)
      {
        _Y.SetDegree(1);
        _Y.SetFactor(0, 0.5);
        _Y.SetFactor(1, y / 2);
      }
      else
        _Y = _generator.ShapeFunction(yp);

      if (zp == 1)
      {
        _Z.SetDegree(1);
        _Z.SetFactor(0, 0.5);
        _Z.SetFactor(1, z / 2);
      }
      else
        _Z = _generator.ShapeFunction(zp);

      _dX = _X.Derivative();
      _dY = _Y.Derivative();
      _dZ = _Z.Derivative();
    }

    HierarchicalShapeFunction::~HierarchicalShapeFunction() {}

    double HierarchicalShapeFunction::F(double x, double y, double z)
    {
      return _X.F(x) * _Y.F(y) * _Z.F(z);
    }

    double HierarchicalShapeFunction::F(algebra::Vec3& v)
    {
      return _X.F(v.X()) * _Y.F(v.Y()) * _Z.F(v.Z());
    }

    double HierarchicalShapeFunction::dxF(double x, double y, double z)
    {
      return _dX.F(x) * _Y.F(y) * _Z.F(z);
    }

    double HierarchicalShapeFunction::dyF(double x, double y, double z)
    {
      return _X.F(x) * _dY.F(y) * _Z.F(z);
    }

    double HierarchicalShapeFunction::dzF(double x, double y, double z)
    {
      return _X.F(x) * _Y.F(y) * _dZ.F(z);
    }

    algebra::Vec3 HierarchicalShapeFunction::dF(double x, double y, double z)
    {
      algebra::Vec3 res;
      res[0] = dxF(x, y, z);
      res[1] = dyF(x, y, z);
      res[2] = dzF(x, y, z);

      return res;
    }

  }

}