#pragma once

#include <cmath>

#include "LegendrePolynomialGenerator.h"

namespace fem
{

  namespace integrals
  {

    class ShapeFunctionGenerator
    {
    public:
      ShapeFunctionGenerator() {}
      virtual ~ShapeFunctionGenerator() {}

      virtual Polynomial ShapeFunction(int p) {
        Polynomial intlp = _LPG.LegendrePolynom(p - 1).Integral();
        
        intlp.AddFactor(0, -1.0 * intlp.F(-1.0));

        return intlp * std::sqrt((2.0 * p - 1.0) / 2.0);
      }

    protected:
      LegendrePolynomialGenerator _LPG;
    };

  }

}