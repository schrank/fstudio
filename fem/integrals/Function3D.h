#pragma once

#include "Function.h"

#include "../../algebra/Vec3.h"
#include "../../algebra/Vector.h"

namespace fem
{

  namespace integrals
  {

    class Function3D:
      virtual public Function
    {
    public:
      Function3D(void) {};
      virtual ~Function3D() {};

      // function value at point (x,y,z)
      virtual double F(double x, double y, double z) = 0;
      virtual double F(algebra::Vec3& v) = 0;

      // derivate vector  at point (x,y,z)
      virtual algebra::Vec3 dF(double x, double y, double z) = 0;

      virtual double dxF(double x, double y, double z) = 0;
      virtual double dyF(double x, double y, double z) = 0;
      virtual double dzF(double x, double y, double z) = 0;
    };

  }

}
