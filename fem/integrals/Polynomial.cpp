#include "Polynomial.h"

#include <iostream>
#include <cmath>
using namespace std;

namespace fem
{

  namespace integrals
  {
    Polynomial::Polynomial() : _degree(0), _factors(nullptr) {}

    Polynomial::Polynomial(int degree) : _degree(degree), _factors(nullptr)
    {
      if (degree >= 0)
      {
        _factors = new double[degree + 1];
        for (int i = 0; i < degree + 1; i++)
          _factors[i] = 0.0;
      }
      else
      {
        cerr << "Error: Wrong polynom degree" << endl;
        exit(1);
      }
    }

    Polynomial::Polynomial(const Polynomial& P)
    {
      _degree = P._degree;
      _factors = new double[_degree + 1];
      for (int i = 0; i <= _degree; i++)
        _factors[i] = P._factors[i];
    }

    Polynomial::~Polynomial(void)
    {
      if (_factors)
        delete[] _factors;
    }

    Polynomial& Polynomial::operator=(const Polynomial& P)
    {
      if (_factors)
        delete[] _factors;

      _degree = P._degree;
      _factors = new double[_degree + 1];
      for (int i = 0; i <= _degree; i++)
        _factors[i] = P._factors[i];

      return *this;
    }

    Polynomial Polynomial::operator+(Polynomial P)
    {
      Polynomial result(max(_degree, P._degree));

      for (int i = 0; i <= _degree; i++)
        result.SetFactor(i, _factors[i]);

      for (int i = 0; i <= P._degree; i++)
        result.SetFactor(i, result._factors[i] + P._factors[i]);

      return result;
    }

    Polynomial Polynomial::operator-(Polynomial P)
    {
      Polynomial result(max(_degree, P._degree));
      for (int i = 0; i <= _degree; i++)
        result.SetFactor(i, _factors[i]);

      for (int i = 0; i <= P._degree; i++)
        result.SetFactor(i, result._factors[i] - P._factors[i]);

      return result;
    }

    Polynomial Polynomial::operator*(Polynomial P)
    {
      Polynomial result(_degree + P._degree);

      for (int i = 0; i <= _degree; i++)
        for (int j = 0; j <= P._degree; j++)
          result.AddFactor(i + j, _factors[i] * P._factors[j]);

      return result;
    }

    Polynomial Polynomial::operator*(double a)
    {
      Polynomial result(_degree);
      for (int i = 0; i <= _degree; i++)
        result.SetFactor(i, _factors[i] * a);

      return result;
    }

    void Polynomial::SetFactor(int degree, double factor)
    {
      _factors[degree] = factor;
    }

    double Polynomial::GetFactor(int degree)
    {
      return _factors[degree];
    }

    void Polynomial::AddFactor(int degree, double factor)
    {
      _factors[degree] += factor;
    }

    void Polynomial::MultFactor(int degree, double factor)
    {
      _factors[degree] *= factor;
    }

    double Polynomial::F(double x)
    {
      double result = _factors[_degree];
      for (int i = _degree - 1; i >= 0; i--)
      {
        result *= x;
        result += _factors[i];
      }

      return result;
    }

    // integral
    Polynomial Polynomial::Integral()
    {
      Polynomial SP(_degree + 1);
      for (int i = _degree; i >= 0; i--)
        SP._factors[i + 1] = _factors[i] / (i + 1);

      return SP;
    }

    // derivative
    Polynomial Polynomial::Derivative(void)
    {
      Polynomial DP(max(_degree - 1, _degree));
      if (_degree)
        for (int i = 0; i < _degree; i++)
          DP._factors[i] = _factors[i + 1] * (i + 1);

      return DP;
    }

    void Polynomial::Print(void)
    {
      for (int i = _degree; i >= 0; i--)
        cout << _factors[i] << "*x^" << i << " ";
      cout << endl;
    }

    void Polynomial::SetDegree(int p)
    {
      if (_factors)
        delete[] _factors;

      if (p >= 0)
      {
        _degree = p;
        _factors = new double[_degree + 1];

        for (int i = 0; i <= _degree; i++)
          _factors[i] = 0.0;
      }
      else
      {
        cerr << "Error: Wrong polynom degree" << endl;
        exit(1);
      }
    }

  }

}