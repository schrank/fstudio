#pragma once

#include <array>

#include "../../algebra/BaseMatrix.h"
#include "../../algebra/Matrix.h"
#include "../FEMOptions.h"

#include "GaussLegendreIntegral.h"
#include "HierarchicalShapeFunction.h"

namespace fem
{
	namespace integrals
	{

		class MatrixStressIntegral:
			virtual public GaussLegendreIntegral
		{
		public:
			MatrixStressIntegral();
			virtual ~MatrixStressIntegral();

			std::shared_ptr<algebra::BaseMatrix> ComputeStiffnessMatrix(unsigned int numGaussPoints);
      std::shared_ptr<algebra::BaseMatrix> ComputeMassMatrix(unsigned int numGaussPoints);
			void SetNodes(std::array<algebra::Vec3, 8> nodes);
			void SetDMatrix(std::shared_ptr<algebra::BaseMatrix> DMatrix);

		private:
			void addNodeMatrixToB(algebra::BaseMatrix& B, algebra::Vec3& dN, int index);
      void addNodeMatrixToN(algebra::BaseMatrix& N, double N_i, int index);

			void computeJacobian(unsigned int x, unsigned int y, unsigned int z);
			//// ��������� ������� ���������
			//numeric::AlgebraUSymMatrix* IntegrateMatrix(uchar numgauss);
			//// ��������� ������� ��� � ������ ����� �����������
			//numeric::AlgebraVector* IntegrateVector(uchar numgauss, uchar face, numeric::AlgebraVector3D& P);
			//// ��������� ������� �������� ���
			//numeric::AlgebraVector* IntegrateVolumeVector(uchar numgauss, numeric::AlgebraVector3D& P);
			//// ���������� ����������
			//numeric::AlgebraVector* Strain(uchar node);
			//// ���������� ����������� � ���� (node=0-7)
			//numeric::AlgebraVector3D* Displacement(uchar node);
			//// ���������� ���������� �� ����� (gp=(0.0,1.0), edge=0-11)
			//numeric::AlgebraVector3D* EdgeDisplacement(double gp, unsigned char edge);

			//void set_Displacement(numeric::AlgebraVector* u);

			//numeric::AlgebraVector* get_Displacement();

			//double EnergyNorm(numeric::AlgebraVector* u, uchar numgauss);
			////! computing only Jacoby determinant without Inverse Jacoby Matrix
			//virtual void ComputeJacobianDeterminant(uchar x, uchar y, uchar z);

			//virtual numeric::AlgebraUSymMatrix* ComputeMassMatrix(uchar numgauss);

			//virtual void AddNodeMatrixToN(numeric::AlgebraMatrix& N, double N_i, int index);

			//virtual void ComputeStiffnessAndMassMatrix(uchar numgauss, numeric::AlgebraUSymMatrix& stiff, numeric::AlgebraUSymMatrix& mass);

			//virtual void AddNodeMatrixToBNotPacked(numeric::AlgebraMatrix& B, numeric::AlgebraVector3D& dN, int index);

			//void ComputeB(uchar numgauss, numeric::AlgebraColPackedMatrix& B);

			//void ComputeB(numeric::AlgebraColPackedMatrix& B);

			//numeric::AlgebraMatrix InverseJacobian();

			//double HexVolume();

		protected:
			// Local Shape Function Of Element
			static HierarchicalShapeFunction shapes[400];
      // Global Node Coordinates
      std::array<algebra::Vec3, 8> _nodes;

			std::shared_ptr<algebra::Matrix> _D;
			// Jacobian
			std::shared_ptr<algebra::Matrix> _J;
			// inverse Jacobian
			std::shared_ptr<algebra::Matrix> _invJ;
			// Jacobian determinant
			double _detJ;

			//// Current Jacobian Determinant
			//double m_Determinant;
			//// Current Inverse Jacobian Rows
			//numeric::AlgebraVector3D m_InverseJacobian[3];

			//numeric::AlgebraVector3D m_Jacobian[3];

			//numeric::AlgebraUSymMatrix* m_D;
			//// ������������� �������� ����������� �� ������, ������ � ���������
			//// ������ ������� 19 (12 �����, 6 ������, 1 �������)
			//numeric::AlgebraIntVector m_Degrees;

			//ushort m_NumberOfNodes;
			//// �������� ����������� (���������� ��� ���������� ���������� �
			//// �������������� �����)
			//numeric::AlgebraVector* m_Displacement;
			//// ������� ���������� �������� � �����
			//void ComputeJacobian(uchar x, uchar y, uchar z);

			//void ComputeJacobian(numeric::AlgebraVector3D& v);

			//numeric::AlgebraIntVector Face(uchar face);

			//void AddNodeMatrixToB(numeric::AlgebraColPackedMatrix& B, numeric::AlgebraVector3D& dN, int index);

		};
	}
}