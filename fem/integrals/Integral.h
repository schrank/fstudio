#pragma once

#include <array>
#include <memory>

#include "../../algebra/BaseMatrix.h"
#include "../../algebra/Vec3.h"

namespace fem
{
  namespace integrals
  {

    class Integral
    {
    public:
      Integral() {}
      virtual ~Integral() {}
    };

  }
}