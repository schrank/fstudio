#include "MatrixStressIntegral.h"

#include "../../algebra/Functions.h"
#include "../../algebra/Matrix.h"

namespace fem
{

  namespace integrals
  {

    MatrixStressIntegral::MatrixStressIntegral() :
      GaussLegendreIntegral(),
      _D(nullptr), _J(nullptr), _invJ(nullptr), _detJ(0.0) {}

    MatrixStressIntegral::~MatrixStressIntegral() {}

    std::shared_ptr<algebra::BaseMatrix> MatrixStressIntegral::ComputeStiffnessMatrix(unsigned int numGaussPoints)
    {
      setNumberGaussPoints(numGaussPoints);

      unsigned int numEqus = _nodes.size() * NUMDOF;

      algebra::Matrix BtDB(numEqus, numEqus);
      algebra::Matrix B(6, numEqus);
      algebra::Matrix K(numEqus, numEqus);

      for (unsigned int ir = 0; ir < numGaussPoints; ir++)
        for (unsigned int is = 0; is < numGaussPoints; is++)
          for (unsigned int it = 0; it < numGaussPoints; it++)
          {
            computeJacobian(ir, is, it);
            
            int index = 0;
            for (int i = 0; i < _nodes.size(); i++, index++)
            {
              algebra::Vec3 dN =
                shapes[i].dF(_gaussPoints[ir]._coord, _gaussPoints[is]._coord, _gaussPoints[it]._coord);

              addNodeMatrixToB(B, dN, index);
            }

            double V = _detJ * _gaussPoints[ir]._length * _gaussPoints[is]._length * _gaussPoints[it]._length;

            BtDB = B.Transpose() * (*_D) * B;
            BtDB *= V;

            K += BtDB;
          }

      return std::make_shared<algebra::Matrix>(K);
    }

    std::shared_ptr<algebra::BaseMatrix> MatrixStressIntegral::ComputeMassMatrix(unsigned int numGaussPoints)
    {
      setNumberGaussPoints(numGaussPoints);

      unsigned int numEqus = _nodes.size() * NUMDOF;

      algebra::Matrix NtN(numEqus, numEqus);
      algebra::Matrix M(numEqus, numEqus);

      for (unsigned int ir = 0; ir < numGaussPoints; ir++)
        for (unsigned int is = 0; is < numGaussPoints; is++)
          for (unsigned int it = 0; it < numGaussPoints; it++)
          {
            algebra::Matrix N(3, numEqus);

            for (int i = 0, index = 0; i < _nodes.size(); i++, index++)
            {
              double N_i = shapes[i].F(_gaussPoints[ir]._coord, _gaussPoints[is]._coord, _gaussPoints[it]._coord);
              
              addNodeMatrixToN(N, N_i, index);
            }

            NtN = N.Transpose() * N;

            double V = _detJ * _gaussPoints[ir]._length * _gaussPoints[is]._length * _gaussPoints[it]._length;
            NtN *= V;

            M += NtN;
          }

      return std::make_shared<algebra::Matrix>(M);
    }

    void MatrixStressIntegral::SetNodes(std::array<algebra::Vec3, 8> nodes)
    {
      _nodes = nodes;
    }

    void MatrixStressIntegral::SetDMatrix(std::shared_ptr<algebra::BaseMatrix> DMatrix)
    {
      _D = std::dynamic_pointer_cast<algebra::Matrix>(DMatrix);
    }

    void MatrixStressIntegral::addNodeMatrixToB(algebra::BaseMatrix& B, algebra::Vec3& dN, int index)
    {
      double dx = 0.0, dy = 0.0, dz = 0.0;

      if (_invJ->Cols() == 3)
      {
        for (int i = 0; i < 3; i++)
        {
          dx += _invJ->Row(0)(i) * dN[i];
          dy += _invJ->Row(1)(i) * dN[i];
          dz += _invJ->Row(2)(i) * dN[i];
        }
      }
      else
        throw std::logic_error("MatrixStressIntegral::addNodeMatrixToB: invJ cols != 3!");

      int col = index * 3;
      B(0, col) = dx;
                        B(1, col + 1) = dy;
                                              B(2, col + 2) = dz;
      B(3, col) = dy;   B(3, col + 1) = dx;
                        B(4, col + 1) = dz;   B(4, col + 2) = dy;
      B(5, col) = dz;                         B(5, col + 2) = dx;
    }

    void MatrixStressIntegral::addNodeMatrixToN(algebra::BaseMatrix& N, double N_i, int index)
    {
      int col = index * NUMDOF;
      N(0, col) = N_i;
      N(1, col + 1) = N_i;
      N(2, col + 2) = N_i;
    }

    void MatrixStressIntegral::computeJacobian(unsigned int x, unsigned int y, unsigned int z)
    {
      _J->Resize(3, 3);

      for (int k = 0; k < 8; k++)
      {
        algebra::Vec3 dN = shapes[k].dF(_gaussPoints[x]._coord, _gaussPoints[y]._coord, _gaussPoints[z]._coord);
        
        for (int i = 0; i < 3; i++)
          for (int j = 0; j < 3; j++)
            (*_J)(i, j) += dN[i] * _nodes[k][j];
      }
      
      _detJ = _J->Determinant();

      _invJ = algebra::Inverse3(_J);
    }

    HierarchicalShapeFunction MatrixStressIntegral::shapes[400] =
    {
      // node shapes
      HierarchicalShapeFunction(1,1,1,local[0][0],local[0][1],local[0][2]),
      HierarchicalShapeFunction(1,1,1,local[1][0],local[1][1],local[1][2]),
      HierarchicalShapeFunction(1,1,1,local[2][0],local[2][1],local[2][2]),
      HierarchicalShapeFunction(1,1,1,local[3][0],local[3][1],local[3][2]),
      HierarchicalShapeFunction(1,1,1,local[4][0],local[4][1],local[4][2]),
      HierarchicalShapeFunction(1,1,1,local[5][0],local[5][1],local[5][2]),
      HierarchicalShapeFunction(1,1,1,local[6][0],local[6][1],local[6][2]),
      HierarchicalShapeFunction(1,1,1,local[7][0],local[7][1],local[7][2]),
      // edge 1 shapes
      HierarchicalShapeFunction(1,2,1,local[8][0],local[8][1],local[8][2]),
      HierarchicalShapeFunction(1,3,1,local[8][0],local[8][1],local[8][2]),
      HierarchicalShapeFunction(1,4,1,local[8][0],local[8][1],local[8][2]),
      HierarchicalShapeFunction(1,5,1,local[8][0],local[8][1],local[8][2]),
      HierarchicalShapeFunction(1,6,1,local[8][0],local[8][1],local[8][2]),
      HierarchicalShapeFunction(1,7,1,local[8][0],local[8][1],local[8][2]),
      HierarchicalShapeFunction(1,8,1,local[8][0],local[8][1],local[8][2]),
      HierarchicalShapeFunction(1,9,1,local[8][0],local[8][1],local[8][2]),
      HierarchicalShapeFunction(1,10,1,local[8][0],local[8][1],local[8][2]),
      HierarchicalShapeFunction(1,11,1,local[8][0],local[8][1],local[8][2]),
      // edge 2 shapes
      HierarchicalShapeFunction(1,1,2,local[9][0],local[9][1],local[9][2]),
      HierarchicalShapeFunction(1,1,3,local[9][0],local[9][1],local[9][2]),
      HierarchicalShapeFunction(1,1,4,local[9][0],local[9][1],local[9][2]),
      HierarchicalShapeFunction(1,1,5,local[9][0],local[9][1],local[9][2]),
      HierarchicalShapeFunction(1,1,6,local[9][0],local[9][1],local[9][2]),
      HierarchicalShapeFunction(1,1,7,local[9][0],local[9][1],local[9][2]),
      HierarchicalShapeFunction(1,1,8,local[9][0],local[9][1],local[9][2]),
      HierarchicalShapeFunction(1,1,9,local[9][0],local[9][1],local[9][2]),
      HierarchicalShapeFunction(1,1,10,local[9][0],local[9][1],local[9][2]),
      HierarchicalShapeFunction(1,1,11,local[9][0],local[9][1],local[9][2]),
      // edge 3 shapes
      HierarchicalShapeFunction(1,2,1,local[10][0],local[10][1],local[10][2]),
      HierarchicalShapeFunction(1,3,1,local[10][0],local[10][1],local[10][2]),
      HierarchicalShapeFunction(1,4,1,local[10][0],local[10][1],local[10][2]),
      HierarchicalShapeFunction(1,5,1,local[10][0],local[10][1],local[10][2]),
      HierarchicalShapeFunction(1,6,1,local[10][0],local[10][1],local[10][2]),
      HierarchicalShapeFunction(1,7,1,local[10][0],local[10][1],local[10][2]),
      HierarchicalShapeFunction(1,8,1,local[10][0],local[10][1],local[10][2]),
      HierarchicalShapeFunction(1,9,1,local[10][0],local[10][1],local[10][2]),
      HierarchicalShapeFunction(1,10,1,local[10][0],local[10][1],local[10][2]),
      HierarchicalShapeFunction(1,11,1,local[10][0],local[10][1],local[10][2]),
      // edge 4 shapes
      HierarchicalShapeFunction(1,1,2,local[11][0],local[11][1],local[11][2]),
      HierarchicalShapeFunction(1,1,3,local[11][0],local[11][1],local[11][2]),
      HierarchicalShapeFunction(1,1,4,local[11][0],local[11][1],local[11][2]),
      HierarchicalShapeFunction(1,1,5,local[11][0],local[11][1],local[11][2]),
      HierarchicalShapeFunction(1,1,6,local[11][0],local[11][1],local[11][2]),
      HierarchicalShapeFunction(1,1,7,local[11][0],local[11][1],local[11][2]),
      HierarchicalShapeFunction(1,1,8,local[11][0],local[11][1],local[11][2]),
      HierarchicalShapeFunction(1,1,9,local[11][0],local[11][1],local[11][2]),
      HierarchicalShapeFunction(1,1,10,local[11][0],local[11][1],local[11][2]),
      HierarchicalShapeFunction(1,1,11,local[11][0],local[11][1],local[11][2]),
      // edge 5 shapes
      HierarchicalShapeFunction(2,1,1,local[12][0],local[12][1],local[12][2]),
      HierarchicalShapeFunction(3,1,1,local[12][0],local[12][1],local[12][2]),
      HierarchicalShapeFunction(4,1,1,local[12][0],local[12][1],local[12][2]),
      HierarchicalShapeFunction(5,1,1,local[12][0],local[12][1],local[12][2]),
      HierarchicalShapeFunction(6,1,1,local[12][0],local[12][1],local[12][2]),
      HierarchicalShapeFunction(7,1,1,local[12][0],local[12][1],local[12][2]),
      HierarchicalShapeFunction(8,1,1,local[12][0],local[12][1],local[12][2]),
      HierarchicalShapeFunction(9,1,1,local[12][0],local[12][1],local[12][2]),
      HierarchicalShapeFunction(10,1,1,local[12][0],local[12][1],local[12][2]),
      HierarchicalShapeFunction(11,1,1,local[12][0],local[12][1],local[12][2]),
      // edge 6 shapes
      HierarchicalShapeFunction(2,1,1,local[13][0],local[13][1],local[13][2]),
      HierarchicalShapeFunction(3,1,1,local[13][0],local[13][1],local[13][2]),
      HierarchicalShapeFunction(4,1,1,local[13][0],local[13][1],local[13][2]),
      HierarchicalShapeFunction(5,1,1,local[13][0],local[13][1],local[13][2]),
      HierarchicalShapeFunction(6,1,1,local[13][0],local[13][1],local[13][2]),
      HierarchicalShapeFunction(7,1,1,local[13][0],local[13][1],local[13][2]),
      HierarchicalShapeFunction(8,1,1,local[13][0],local[13][1],local[13][2]),
      HierarchicalShapeFunction(9,1,1,local[13][0],local[13][1],local[13][2]),
      HierarchicalShapeFunction(10,1,1,local[13][0],local[13][1],local[13][2]),
      HierarchicalShapeFunction(11,1,1,local[13][0],local[13][1],local[13][2]),
      // edge 7 shapes
      HierarchicalShapeFunction(2,1,1,local[14][0],local[14][1],local[14][2]),
      HierarchicalShapeFunction(3,1,1,local[14][0],local[14][1],local[14][2]),
      HierarchicalShapeFunction(4,1,1,local[14][0],local[14][1],local[14][2]),
      HierarchicalShapeFunction(5,1,1,local[14][0],local[14][1],local[14][2]),
      HierarchicalShapeFunction(6,1,1,local[14][0],local[14][1],local[14][2]),
      HierarchicalShapeFunction(7,1,1,local[14][0],local[14][1],local[14][2]),
      HierarchicalShapeFunction(8,1,1,local[14][0],local[14][1],local[14][2]),
      HierarchicalShapeFunction(9,1,1,local[14][0],local[14][1],local[14][2]),
      HierarchicalShapeFunction(10,1,1,local[14][0],local[14][1],local[14][2]),
      HierarchicalShapeFunction(11,1,1,local[14][0],local[14][1],local[14][2]),
      // edge 8 shapes
      HierarchicalShapeFunction(2,1,1,local[15][0],local[15][1],local[15][2]),
      HierarchicalShapeFunction(3,1,1,local[15][0],local[15][1],local[15][2]),
      HierarchicalShapeFunction(4,1,1,local[15][0],local[15][1],local[15][2]),
      HierarchicalShapeFunction(5,1,1,local[15][0],local[15][1],local[15][2]),
      HierarchicalShapeFunction(6,1,1,local[15][0],local[15][1],local[15][2]),
      HierarchicalShapeFunction(7,1,1,local[15][0],local[15][1],local[15][2]),
      HierarchicalShapeFunction(8,1,1,local[15][0],local[15][1],local[15][2]),
      HierarchicalShapeFunction(9,1,1,local[15][0],local[15][1],local[15][2]),
      HierarchicalShapeFunction(10,1,1,local[15][0],local[15][1],local[15][2]),
      HierarchicalShapeFunction(11,1,1,local[15][0],local[15][1],local[15][2]),
      // edge 9 shapes
      HierarchicalShapeFunction(1,1,2,local[16][0],local[16][1],local[16][2]),
      HierarchicalShapeFunction(1,1,3,local[16][0],local[16][1],local[16][2]),
      HierarchicalShapeFunction(1,1,4,local[16][0],local[16][1],local[16][2]),
      HierarchicalShapeFunction(1,1,5,local[16][0],local[16][1],local[16][2]),
      HierarchicalShapeFunction(1,1,6,local[16][0],local[16][1],local[16][2]),
      HierarchicalShapeFunction(1,1,7,local[16][0],local[16][1],local[16][2]),
      HierarchicalShapeFunction(1,1,8,local[16][0],local[16][1],local[16][2]),
      HierarchicalShapeFunction(1,1,9,local[16][0],local[16][1],local[16][2]),
      HierarchicalShapeFunction(1,1,10,local[16][0],local[16][1],local[16][2]),
      HierarchicalShapeFunction(1,1,11,local[16][0],local[16][1],local[16][2]),
      // edge 10 shapes
      HierarchicalShapeFunction(1,2,1,local[17][0],local[17][1],local[17][2]),
      HierarchicalShapeFunction(1,3,1,local[17][0],local[17][1],local[17][2]),
      HierarchicalShapeFunction(1,4,1,local[17][0],local[17][1],local[17][2]),
      HierarchicalShapeFunction(1,5,1,local[17][0],local[17][1],local[17][2]),
      HierarchicalShapeFunction(1,6,1,local[17][0],local[17][1],local[17][2]),
      HierarchicalShapeFunction(1,7,1,local[17][0],local[17][1],local[17][2]),
      HierarchicalShapeFunction(1,8,1,local[17][0],local[17][1],local[17][2]),
      HierarchicalShapeFunction(1,9,1,local[17][0],local[17][1],local[17][2]),
      HierarchicalShapeFunction(1,10,1,local[17][0],local[17][1],local[17][2]),
      HierarchicalShapeFunction(1,11,1,local[17][0],local[17][1],local[17][2]),
      // edge 11 shapes
      HierarchicalShapeFunction(1,1,2,local[18][0],local[18][1],local[18][2]),
      HierarchicalShapeFunction(1,1,3,local[18][0],local[18][1],local[18][2]),
      HierarchicalShapeFunction(1,1,4,local[18][0],local[18][1],local[18][2]),
      HierarchicalShapeFunction(1,1,5,local[18][0],local[18][1],local[18][2]),
      HierarchicalShapeFunction(1,1,6,local[18][0],local[18][1],local[18][2]),
      HierarchicalShapeFunction(1,1,7,local[18][0],local[18][1],local[18][2]),
      HierarchicalShapeFunction(1,1,8,local[18][0],local[18][1],local[18][2]),
      HierarchicalShapeFunction(1,1,9,local[18][0],local[18][1],local[18][2]),
      HierarchicalShapeFunction(1,1,10,local[18][0],local[18][1],local[18][2]),
      HierarchicalShapeFunction(1,1,11,local[18][0],local[18][1],local[18][2]),
      // edge 12 shapes
      HierarchicalShapeFunction(1,2,1,local[19][0],local[19][1],local[19][2]),
      HierarchicalShapeFunction(1,3,1,local[19][0],local[19][1],local[19][2]),
      HierarchicalShapeFunction(1,4,1,local[19][0],local[19][1],local[19][2]),
      HierarchicalShapeFunction(1,5,1,local[19][0],local[19][1],local[19][2]),
      HierarchicalShapeFunction(1,6,1,local[19][0],local[19][1],local[19][2]),
      HierarchicalShapeFunction(1,7,1,local[19][0],local[19][1],local[19][2]),
      HierarchicalShapeFunction(1,8,1,local[19][0],local[19][1],local[19][2]),
      HierarchicalShapeFunction(1,9,1,local[19][0],local[19][1],local[19][2]),
      HierarchicalShapeFunction(1,10,1,local[19][0],local[19][1],local[19][2]),
      HierarchicalShapeFunction(1,11,1,local[19][0],local[19][1],local[19][2]),
      // face 1 shapes
      HierarchicalShapeFunction(1,2,2,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,2,3,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,3,2,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,2,4,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,3,3,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,4,2,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,2,5,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,3,4,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,4,3,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,5,2,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,2,6,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,3,5,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,4,4,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,5,3,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,6,2,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,2,7,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,3,6,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,4,5,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,5,4,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,6,3,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,7,2,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,2,8,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,3,7,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,4,6,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,5,5,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,6,4,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,7,3,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,8,2,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,2,9,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,3,8,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,4,7,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,5,6,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,6,5,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,7,4,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,8,3,local[20][0],local[20][1],local[20][2]),
      HierarchicalShapeFunction(1,9,2,local[20][0],local[20][1],local[20][2]),
      // face 2 shapes
      HierarchicalShapeFunction(2,2,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(2,3,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(3,2,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(2,4,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(3,3,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(4,2,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(2,5,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(3,4,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(4,3,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(5,2,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(2,6,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(3,5,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(4,4,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(5,3,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(6,2,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(2,7,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(3,6,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(4,5,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(5,4,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(6,3,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(7,2,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(2,8,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(3,7,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(4,6,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(5,5,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(6,4,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(7,3,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(8,2,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(2,9,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(3,8,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(4,7,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(5,6,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(6,5,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(7,4,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(8,3,1,local[21][0],local[21][1],local[21][2]),
      HierarchicalShapeFunction(9,2,1,local[21][0],local[21][1],local[21][2]),
      // face 3 shapes
      HierarchicalShapeFunction(2,1,2,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(2,1,3,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(3,1,2,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(2,1,4,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(3,1,3,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(4,1,2,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(2,1,5,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(3,1,4,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(4,1,3,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(5,1,2,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(2,1,6,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(3,1,5,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(4,1,4,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(5,1,3,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(6,1,2,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(2,1,7,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(3,1,6,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(4,1,5,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(5,1,4,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(6,1,3,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(7,1,2,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(2,1,8,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(3,1,7,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(4,1,6,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(5,1,5,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(6,1,4,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(7,1,3,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(8,1,2,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(2,1,9,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(3,1,8,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(4,1,7,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(5,1,6,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(6,1,5,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(7,1,4,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(8,1,3,local[22][0],local[22][1],local[22][2]),
      HierarchicalShapeFunction(9,1,2,local[22][0],local[22][1],local[22][2]),
      // face 4 shapes
      HierarchicalShapeFunction(2,2,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(2,3,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(3,2,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(2,4,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(3,3,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(4,2,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(2,5,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(3,4,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(4,3,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(5,2,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(2,6,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(3,5,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(4,4,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(5,3,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(6,2,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(2,7,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(3,6,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(4,5,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(5,4,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(6,3,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(7,2,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(2,8,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(3,7,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(4,6,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(5,5,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(6,4,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(7,3,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(8,2,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(2,9,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(3,8,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(4,7,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(5,6,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(6,5,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(7,4,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(8,3,1,local[23][0],local[23][1],local[23][2]),
      HierarchicalShapeFunction(9,2,1,local[23][0],local[23][1],local[23][2]),
      // face 5 shapes
      HierarchicalShapeFunction(2,1,2,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(2,1,3,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(3,1,2,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(2,1,4,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(3,1,3,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(4,1,2,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(2,1,5,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(3,1,4,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(4,1,3,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(5,1,2,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(2,1,6,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(3,1,5,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(4,1,4,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(5,1,3,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(6,1,2,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(2,1,7,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(3,1,6,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(4,1,5,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(5,1,4,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(6,1,3,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(7,1,2,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(2,1,8,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(3,1,7,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(4,1,6,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(5,1,5,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(6,1,4,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(7,1,3,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(8,1,2,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(2,1,9,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(3,1,8,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(4,1,7,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(5,1,6,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(6,1,5,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(7,1,4,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(8,1,3,local[24][0],local[24][1],local[24][2]),
      HierarchicalShapeFunction(9,1,2,local[24][0],local[24][1],local[24][2]),
      // face 6 shapes
      HierarchicalShapeFunction(1,2,2,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,2,3,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,3,2,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,2,4,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,3,3,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,4,2,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,2,5,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,3,4,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,4,3,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,5,2,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,2,6,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,3,5,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,4,4,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,5,3,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,6,2,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,2,7,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,3,6,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,4,5,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,5,4,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,6,3,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,7,2,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,2,8,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,3,7,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,4,6,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,5,5,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,6,4,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,7,3,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,8,2,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,2,9,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,3,8,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,4,7,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,5,6,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,6,5,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,7,4,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,8,3,local[25][0],local[25][1],local[25][2]),
      HierarchicalShapeFunction(1,9,2,local[25][0],local[25][1],local[25][2]),

      // internal element shapes
      HierarchicalShapeFunction(2,2,2,local[26][0],local[26][1],local[26][2]),

      HierarchicalShapeFunction(2,2,3,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(2,3,2,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(3,2,2,local[26][0],local[26][1],local[26][2]),

      HierarchicalShapeFunction(2,2,4,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(2,4,2,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(4,2,2,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(2,3,3,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(3,3,2,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(3,2,3,local[26][0],local[26][1],local[26][2]),

      HierarchicalShapeFunction(2,2,5,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(2,3,4,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(2,4,3,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(2,5,2,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(3,2,4,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(3,3,3,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(3,4,2,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(4,2,3,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(4,3,2,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(5,2,2,local[26][0],local[26][1],local[26][2]),

      HierarchicalShapeFunction(2,2,6,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(2,3,5,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(2,4,4,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(2,5,3,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(2,6,2,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(3,2,5,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(3,3,4,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(3,4,3,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(3,5,2,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(4,2,4,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(4,3,3,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(4,4,2,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(5,2,3,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(5,3,2,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(6,2,2,local[26][0],local[26][1],local[26][2]),

      HierarchicalShapeFunction(2,2,7,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(2,3,6,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(2,4,5,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(2,5,4,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(2,6,3,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(2,7,2,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(3,2,6,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(3,3,5,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(3,4,4,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(3,5,3,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(3,6,2,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(4,2,5,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(4,3,4,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(4,4,3,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(4,5,2,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(5,2,4,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(5,3,3,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(5,4,2,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(6,2,3,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(6,3,2,local[26][0],local[26][1],local[26][2]),
      HierarchicalShapeFunction(7,2,2,local[26][0],local[26][1],local[26][2])
    };

  }

}