#pragma once

#include "Polynomial.h"

namespace fem
{

  namespace integrals
  {

    class LegendrePolynomialGenerator
    {
    public:
      LegendrePolynomialGenerator(): _legendre0(0), _legendre1(1) {
        _legendre0.SetFactor(0, 1.0);
        _legendre1.SetFactor(1, 1.0);
      }
      virtual ~LegendrePolynomialGenerator() {}

      virtual Polynomial LegendrePolynom(int p) {
        if (p == 0)
          return _legendre0;
        else
        {
          if (p == 1)
            return _legendre1;
          else
          {
            Polynomial c1(1);
            c1.SetFactor(1, (2.0 * p - 1.0) / double(p));
            Polynomial c2(0);
            c2.SetFactor(0, (p - 1.0) / double(p));
            return c1 * LegendrePolynom(p - 1) - c2 * LegendrePolynom(p - 2);
          }
        }
      }

    protected:
      Polynomial _legendre0;
      Polynomial _legendre1;
    };

  }

}