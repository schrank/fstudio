#pragma once

#include <vector>

#include "Integral.h"

namespace fem
{
  namespace integrals
  {

    struct GaussPoint
    {
      double _coord;
      double _length;
    };

    class GaussLegendreIntegral:
      virtual public Integral
    {
    public:
      GaussLegendreIntegral();
      virtual ~GaussLegendreIntegral();

      std::vector<GaussPoint>& GetGaussPoints();
      unsigned int GetNumberGaussPoints();

    protected:
      void createGaussPoints();
      void setNumberGaussPoints(unsigned int num);

    protected:
      static double local[27][3];

      std::vector<GaussPoint> _gaussPoints;
    };

  }
}
