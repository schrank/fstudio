#pragma once

#include "Function1D.h"

namespace fem
{

  namespace integrals
  {

    class Polynomial:
      virtual public Function1D
    {
    public:
      Polynomial();

      Polynomial(int degree);

      Polynomial(const Polynomial& P);

      virtual ~Polynomial();

      virtual Polynomial& operator=(const Polynomial& P);

      virtual Polynomial operator+(Polynomial P);

      virtual Polynomial operator-(Polynomial P);

      virtual Polynomial operator*(Polynomial P);

      virtual Polynomial operator*(double a);

      virtual void SetFactor(int degree, double factor);

      double GetFactor(int degree);

      virtual void AddFactor(int degree, double factor);

      virtual void MultFactor(int degree, double factor);

      virtual double F(double x);

      // integral
      virtual Polynomial Integral(void);

      // derivative
      virtual Polynomial Derivative(void);

      virtual void Print(void);

      virtual void SetDegree(int p);

    protected:
      int _degree;

      // polynom coefficients
      double* _factors;
    };

  }

}