#pragma once

#include "Function3D.h"
#include "Polynomial.h"
#include "ShapeFunctionGenerator.h"

namespace fem
{

  namespace integrals
  {
    // Shape function for node (xk, yk, zk). (xp,yp,zp)- degrees of polynomials in corresponding direction
    class HierarchicalShapeFunction:
      virtual public Function3D
    {
    public:
      HierarchicalShapeFunction();
      HierarchicalShapeFunction(int xp, int yp, int zp, double x, double y, double z);
      virtual ~HierarchicalShapeFunction();

      double F(double x, double y, double z) override;
      double F(algebra::Vec3& v) override;

      algebra::Vec3 dF(double x, double y, double z) override;

      double dxF(double x, double y, double z) override;
      double dyF(double x, double y, double z) override;
      double dzF(double x, double y, double z) override;

    protected:
      Polynomial _X;
      Polynomial _Y;
      Polynomial _Z;

      Polynomial _dX;
      Polynomial _dY;
      Polynomial _dZ;

      ShapeFunctionGenerator _generator;
    };

  }

}