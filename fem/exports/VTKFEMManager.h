#pragma once

#include <string>

#include "../../mesh/expotrs/VTKManager.h"
#include "../mesh/FEMesh.h"

namespace fem
{

  namespace exports
  {

    class VTKFEMManager:
      public virtual mesh::exports::VTKManager
    {
    public:
      VTKFEMManager() {}

      virtual ~VTKFEMManager() {}

      void Export(std::shared_ptr<femesh::FEMesh> mesh, std::string filename);

    protected:
      void pointsData(std::stringstream& out, std::shared_ptr<fem::femesh::FEMesh> mesh) const;
      void cellsData(std::stringstream& out, std::shared_ptr<fem::femesh::FEMesh> mesh) const;
    };

  }
}