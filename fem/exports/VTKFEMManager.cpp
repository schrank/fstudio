#include "VTKFEMManager.h"

#include <fstream>
#include <map>
#include <set>

#include "../mesh/FEMesh.h"
#include "../../mesh/mesh/Node.h"
#include "../../mesh/mesh/Primitive.h"

namespace fem
{
	namespace exports
	{
		void VTKFEMManager::Export(std::shared_ptr<femesh::FEMesh> mesh, std::string filename)
		{
			std::ofstream outfile(filename.c_str());

			if (outfile.is_open())
			{
				std::stringstream out;

				header(out, mesh);
				outfile << out.rdbuf();
				out.str("");

				points(out, mesh);
				outfile << out.rdbuf();
				out.str("");

				pointsData(out, mesh);
				outfile << out.rdbuf();
				out.str("");

				cells(out, mesh);
				outfile << out.rdbuf();
				out.str("");

				cellsData(out, mesh);
				outfile << out.rdbuf();
				out.str("");

				footer(out, mesh);
				outfile << out.rdbuf();
				out.str("");

				outfile.close();
			}
			else
				throw std::logic_error("VTKManager::Export: Error with mesh file!");
		}
		
		void VTKFEMManager::pointsData(std::stringstream& out, std::shared_ptr<fem::femesh::FEMesh> mesh) const
		{
			std::map<std::string, std::string> propMap;

			std::set<std::string> propertyNames;
			for (mesh::NodePtr node : mesh->GetNodes())
				for (mesh::PropPtr prop : node->GetProperties())
					propertyNames.insert(prop->GetName());

			for (mesh::NodePtr node : mesh->GetNodes())
				for (std::string pName : propertyNames)
				{
					std::string result = (!node->GetProperty(pName)) ?
						node->GetProperty(pName)->GetData() :
						EmptyData(pName);

					propMap[pName] += result;
				}
	
			out << "      <PointData Scalars=\"scalars\">\n";

			using ci = std::map<std::string, std::string>::const_iterator;
			for (ci it = propMap.begin(); it != propMap.end(); it++)
				dataArrayNode(out, it->second, "Float64", it->first, PropertyField[it->first]);

		  out << "      </PointData>\n";
		}

		void VTKFEMManager::cellsData(std::stringstream& out, std::shared_ptr<fem::femesh::FEMesh> mesh) const
		{
			std::map<std::string, std::string> propMap;

			std::set<std::string> propertyNames;
			for (mesh::PrimitivePtr cell : mesh->GetCells())
				for (mesh::PropPtr prop : cell->GetProperties())
					propertyNames.insert(prop->GetName());

			for (::mesh::PrimitivePtr cell : mesh->GetCells())
				for (std::string pName : propertyNames)
				{
					std::string result = (cell->GetProperty(pName)) ?
						cell->GetProperty(pName)->GetData() :
						EmptyData(pName);

					propMap[pName] += result;
				}

			out << "      <CellData Scalars=\"scalars\">\n";

			using ci = std::map<std::string, std::string>::const_iterator;
			for (ci it = propMap.begin(); it != propMap.end(); it++)
				dataArrayNode(out, it->second, "Float64", it->first, PropertyField[it->first]);

			out << "      </CellData>\n";
		}

	}

}