#include "FEMHexahedron.h"

#include <memory>

#include "../properties/Density.h"
#include "../properties/FEMaterial.h"

namespace fem
{

  namespace femesh
  {

    FEMHexahedron::FEMHexahedron():
      ::mesh::Hexahedron(),
      _integral(nullptr)
    {
      init("FEMHexahedron");

      initMaterial();
      initIntegral();
    }

    FEMHexahedron::FEMHexahedron(std::vector<std::shared_ptr<::mesh::Node>> nodes) :
      ::mesh::Hexahedron(nodes),
      _integral(nullptr)
    {
      init("FEMHexahedron");

      initFields();

      initMaterial();
      initIntegral();
    }

    FEMHexahedron::~FEMHexahedron() {}

    void FEMHexahedron::FormMassMatrix()
    {
      _M = _integral->ComputeMassMatrix(3);

      std::shared_ptr<prop::Density> density =
        std::dynamic_pointer_cast<prop::Density>(GetProperty("Density"));

      if(density)
        *_M *= density->GetDensity();

      //// ������������ ������� ����
      //if (options.typeOfMassMatrix == diagonal)
      //{
      //  int cols = m_MassMatrix->Cols();
      //  for (int i = 0; i < cols; i++)
      //  {
      //    double sum = 0.0;
      //    for (int j = 0; j < cols; j++)
      //      sum += (*m_MassMatrix)(i, j);
      //    (*m_MassMatrix)(i, i) = sum;
      //  }
      //  for (int i = 0; i < cols; i++)
      //    for (int j = i + 1; j < cols; j++)
      //      (*m_MassMatrix)(i, j) = 0;
      //}

      //if (options.typeOfDynamicSolver != Newmark)
      //  HandleMatrixConstraints();
    }

    void FEMHexahedron::FormStiffnessMatrix()
    {
      _K = _integral->ComputeStiffnessMatrix(3);

      //if (options.typeOfDynamicSolver != Newmark)
      //  HandleMatrixConstraints();
    }

    void FEMHexahedron::FormResistingForceVector()
    {

    }

    void FEMHexahedron::FormExternalForceVector()
    {

    }

    void FEMHexahedron::initIntegral()
    {
      std::array<algebra::Vec3, 8> nodes;

      for (int i = 0; i < 8; i++)
      {
        nodes[i][0] = _nodes[i]->X();
        nodes[i][1] = _nodes[i]->Y();
        nodes[i][2] = _nodes[i]->Z();
      }

      _integral = std::make_shared<integrals::MatrixStressIntegral>();
      _integral->SetNodes(nodes);
    }

    void FEMHexahedron::initMaterial()
    {
      std::shared_ptr<prop::FEMaterial> material =
        std::dynamic_pointer_cast<prop::FEMaterial>(GetProperty("Material"));

      if(material)
        material->FormDMatrix();
    }

  }

}