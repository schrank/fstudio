#include "FEMesh.h"

#include "FEMPrimitive.h"

namespace fem
{

  namespace femesh
  {
   
    FEMesh::FEMesh() {}
    FEMesh::~FEMesh() {}

    void FEMesh::FormMassMatrix()
    {
      for (mesh::PrimitivePtr cell : GetCells())
      {
        std::shared_ptr<FEMPrimitive> feCell = std::dynamic_pointer_cast<FEMPrimitive>(cell);

        if (feCell)
          feCell->FormMassMatrix();
      }
    }

    void FEMesh::FormStiffnessMatrix()
    {
      for (mesh::PrimitivePtr cell : GetCells())
      {
        std::shared_ptr<FEMPrimitive> feCell = std::dynamic_pointer_cast<FEMPrimitive>(cell);

        if (feCell)
          feCell->FormStiffnessMatrix();
      }
    }

    void FEMesh::FormResistingForceVector()
    {

    }

    void FEMesh::FormExternalForceVector()
    {

    }

  }
}