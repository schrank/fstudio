#pragma once

#include <memory>

#include "../../mesh/mesh/Node.h"
#include "../analysis/DOFGroup.h"

namespace fem
{

  namespace femesh
  {

    class FEMNode :
      virtual public ::mesh::Node
    {
    public:
      FEMNode();
      FEMNode(double x, double y, double z);
      virtual ~FEMNode();

      void SetDOFGroup(std::shared_ptr<analysis::DOFGroup> dofgroup);

    private:
      std::shared_ptr<analysis::DOFGroup> _dofGroup;
    };

  }

  using FEMNodePtr = std::shared_ptr<fem::femesh::FEMNode>;

}