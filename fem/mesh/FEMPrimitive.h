#pragma once

#include <memory>

#include "../../mesh/mesh/Primitive.h"

#include "../../algebra/BaseMatrix.h"
#include "../../algebra/Vector.h"

namespace fem
{

  namespace femesh
  {

    class FEMPrimitive :
      virtual public ::mesh::Primitive
    {
    public:
      FEMPrimitive();
      FEMPrimitive(std::vector<::mesh::NodePtr> nodes);      
      virtual ~FEMPrimitive();

      virtual void FormMassMatrix() = 0;
      virtual void FormStiffnessMatrix() = 0;
      virtual void FormResistingForceVector() = 0;
      virtual void FormExternalForceVector() = 0;

      void HandleMatrixConstraints();
      void HandleVectorConstraints();

    protected:
      void initFields();
      void handleMatrixConstraints_UnitDiagonal();
      void handleVectorConstraints_UnitDiagonal();

    protected:
      // mass matrix
      std::shared_ptr<algebra::BaseMatrix> _M;
      // stiffness matrix
      std::shared_ptr<algebra::BaseMatrix> _K;
      // resisting force vector
      std::shared_ptr<algebra::Vector> _G;
      // external force vector
      std::shared_ptr<algebra::Vector> _R;
      // tmp
      std::shared_ptr<algebra::Vector> _eliminationVector;
    };

  }

}