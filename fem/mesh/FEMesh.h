#pragma once

#include "../../mesh/mesh/Mesh.h"
//#include "../exports/VTKFEMManager.h" 

namespace fem
{

  namespace femesh
  {

    class FEMesh :
      virtual public mesh::Mesh
    {
    public:
      FEMesh();
      virtual ~FEMesh();

      void FormMassMatrix();
      void FormStiffnessMatrix();
      void FormResistingForceVector();
      void FormExternalForceVector();
    };

  }

}