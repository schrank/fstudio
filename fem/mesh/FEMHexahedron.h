#pragma once

#include "FEMPrimitive.h"
#include "../../mesh/mesh/Hexahedron.h"
#include "../../mesh/mesh/Node.h"
#include "../integrals/MatrixStressIntegral.h"

namespace fem
{
  namespace femesh
  {

    class FEMHexahedron:
      virtual public mesh::Hexahedron,
      virtual public FEMPrimitive
    {
    public:
      FEMHexahedron();
      FEMHexahedron(std::vector<std::shared_ptr<mesh::Node>> nodes);
      virtual ~FEMHexahedron();

      void FormMassMatrix() override;
      void FormStiffnessMatrix() override;
      void FormResistingForceVector() override;
      void FormExternalForceVector() override;

    private:
      void initIntegral();
      void initMaterial();

    private:
      std::shared_ptr<integrals::MatrixStressIntegral> _integral;
    };

  }

}