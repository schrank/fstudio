#include "FEMNode.h"

namespace fem
{

  namespace femesh
  {

    FEMNode::FEMNode() : ::mesh::Node()
    {
      init("FEMNode");
    }

    FEMNode::FEMNode(double x, double y, double z) : ::mesh::Node(x, y, z)
    {
      init("FEMNode");
    }

    FEMNode::~FEMNode() {}

    void FEMNode::SetDOFGroup(std::shared_ptr<analysis::DOFGroup> dofGroup)
    {
      _dofGroup = dofGroup;
    }

  }

}