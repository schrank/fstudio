#include "FEMPrimitive.h"

#include "../FEMOptions.h"
#include "../properties/SPConstraint.h"

#include "../../algebra/Matrix.h"
#include "../../algebra/RowPackedMatrix.h"

namespace fem
{

  namespace femesh
  {

    FEMPrimitive::FEMPrimitive():
      ::mesh::Primitive()
    {
      init("FEMPrimitive");
    }

    FEMPrimitive::FEMPrimitive(std::vector<::mesh::NodePtr> nodes):
      ::mesh::Primitive(nodes)
    {
      init("FEMPrimitive");
    }

    FEMPrimitive::~FEMPrimitive() {}

    void FEMPrimitive::HandleMatrixConstraints()
    {
      handleMatrixConstraints_UnitDiagonal();
    }

    void FEMPrimitive::HandleVectorConstraints()
    {
      handleVectorConstraints_UnitDiagonal();
    }

    void FEMPrimitive::initFields()
    {
      unsigned int numDOFs = NumberOfNodes() * NUMDOF;

      if (MATRIX_TYPE == Full)
      {
        _M = std::make_shared<algebra::Matrix>(numDOFs, numDOFs);
        _K = std::make_shared<algebra::Matrix>(numDOFs, numDOFs);
      }
      else if (MATRIX_TYPE == Packed)
      {
        _M = std::make_shared<algebra::RowPackedMatrix>(numDOFs, numDOFs);
        _K = std::make_shared<algebra::RowPackedMatrix>(numDOFs, numDOFs);
      }

      _G = std::make_shared<algebra::Vector>(numDOFs);
      _R = std::make_shared<algebra::Vector>(numDOFs);

      _eliminationVector = std::make_shared<algebra::Vector>(numDOFs);
    }

    void FEMPrimitive::handleVectorConstraints_UnitDiagonal()
    {
      int numNodes = NumberOfNodes();
      unsigned int numDOFs = numNodes * NUMDOF;

      for (int l = 0; l < NUMDOF; l++)
        (*_R)(l) += (*_eliminationVector)(l);

      for (int i = 0; i < numNodes; i++)
      {
        ::mesh::NodePtr node = _nodes[i];
        std::shared_ptr<prop::SPConstraint> sp = std::dynamic_pointer_cast<prop::SPConstraint>(node->GetProperty("SPConstraint"));

        if (sp)
        {
          std::map<int, double> constraint = sp->GetConstraint();

          for (std::map<int, double>::const_iterator ci = constraint.begin(); ci != constraint.end(); ci++)
            (*_R)(NUMDOF * i + ci->first) = ci->second;
        }
      }
    }

    void FEMPrimitive::handleMatrixConstraints_UnitDiagonal()
    {
      unsigned int numNodes = NumberOfNodes();
      unsigned int numDOFs = numNodes * NUMDOF;

      _eliminationVector->Zero();

      for (unsigned int i = 0; i < numNodes; i++)
      {
        ::mesh::NodePtr node = _nodes[i];
        std::shared_ptr<prop::SPConstraint> sp = std::dynamic_pointer_cast<prop::SPConstraint>(node->GetProperty("SPConstraint"));

        if (sp)
        {
          std::map<int, double> constraint = sp->GetConstraint();

          // attention! tangent matrix has symmetric format
          // so be careful when vanishing its row, since it causes vanishing of the col
          for (std::map<int, double>::const_iterator ci = constraint.begin(); ci != constraint.end(); ci++)
          {
            for (unsigned int j = 0; j < NUMDOF; j++)
              if (j != (NUMDOF * i + ci->first))
                (*_eliminationVector)(j) -= (*_K)(j, NUMDOF * i + ci->first) * ci->second;

            //zero row = zero col
            _K->ZeroRow(NUMDOF * i + ci->first);
            (*_K)(NUMDOF * i + ci->first, NUMDOF * i + ci->first) = 1.0;
          }
        }
      }

    }

  }

}