#pragma once

namespace fem
{
  
  static short NUMDOF = 3;


  enum MatrixType { Full, Packed };

  static MatrixType MATRIX_TYPE = Full;

}