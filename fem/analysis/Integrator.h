#pragma once

#include "../mesh/FEMesh.h"

namespace fem
{

  namespace analysis
  {

    class Integrator
    {
    public:
      Integrator() {}
      virtual ~Integrator() {}

      virtual void CommitState() = 0;
      virtual void UpdateState() = 0;

      virtual void FormMassMatrix(std::shared_ptr<femesh::FEMesh> femesh) = 0;
      virtual void FormStiffnessMatrix(std::shared_ptr<femesh::FEMesh> femesh) = 0;
      virtual void FormExternalForceVector(std::shared_ptr<femesh::FEMesh> femesh) = 0;
      virtual void FormResistingForceVector(std::shared_ptr<femesh::FEMesh> femesh) = 0;
    };

  }

}