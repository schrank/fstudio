#pragma once

namespace fem
{

  namespace analysis
  {

    class Analysis
    {
    public: 
      Analysis() {}
      virtual ~Analysis() {}

      virtual bool Analyze() = 0;
    };
  }

}