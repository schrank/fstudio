#pragma once

#include <vector>

namespace fem
{

  namespace analysis
  {

    class DOFGroup
    {
    public:
      DOFGroup();
      DOFGroup(std::vector<unsigned int> _eqNumbers);
      DOFGroup(unsigned int tag, std::vector<unsigned int> _eqNumbers);
      ~DOFGroup();

    private:
      long _tag;

      std::vector<unsigned int> _eqNumbers;
      std::vector<unsigned int> _shiftNumbers;
    };

  }

}