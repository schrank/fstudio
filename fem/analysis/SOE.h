#pragma once

#include <memory>

#include "../../algebra/BaseMatrix.h"
#include "../../algebra/Vector.h"

#include "../mesh/FEMesh.h"
namespace fem
{

  namespace analysis
  {

    class SOE
    {
    public:
      SOE() {}
      virtual ~SOE() {}

      virtual void FormSOE(std::shared_ptr<femesh::FEMesh> feMesh) = 0;

    protected:
      std::shared_ptr<algebra::Vector> _b;
    };

  }

}