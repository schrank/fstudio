#pragma once

#include "../mesh/FEMesh.h"
#include "Integrator.h"
#include "SOE.h"
#include "../solver/DynamicSolver.h"
#include "../exports/VTKFEMManager.h"

namespace fem
{

  namespace analysis
  {

    const unsigned int NDOF = 3;

    class AnalysisModel
    {
    public:
      AnalysisModel();
      AnalysisModel(
        std::shared_ptr<femesh::FEMesh> femesh,
        std::shared_ptr<Integrator> integrator,
        std::shared_ptr<SOE> soe,
        std::shared_ptr<solver::DynamicSolver> solver);
        
      ~AnalysisModel();

      void ExportState(std::shared_ptr<exports::VTKFEMManager> writer, std::string fileName);
      void InitState();
      void SaveState();
      void Solve();

      //void IncrementAcceleration(numeric::AlgebraVector* acceleration);
      //void IncrementDisplacement(numeric::AlgebraVector* displacement);
      //void IncrementVelocity(numeric::AlgebraVector* velocity);

    private:
      void createEquationNumbers();

      void updateState();

    private:
      std::shared_ptr<femesh::FEMesh> _femesh;
      std::shared_ptr<Integrator> _integrator;
      std::shared_ptr<SOE> _soe;
      std::shared_ptr<solver::DynamicSolver> _solver;
    };

  }

}