#pragma once

#include "Integrator.h"

namespace fem
{

  namespace analysis
  {

    class IntegratorDynamic final:
      virtual public Integrator
    {
    public:
      IntegratorDynamic() {}
      virtual ~IntegratorDynamic() {}

      void CommitState() override;
      void UpdateState() override;

      void FormStiffnessMatrix(std::shared_ptr<femesh::FEMesh> femesh) override;
      void FormResistingForceVector(std::shared_ptr<femesh::FEMesh> femesh) override;
      void FormMassMatrix(std::shared_ptr<femesh::FEMesh> femesh) override;
      void FormExternalForceVector(std::shared_ptr<femesh::FEMesh> femesh) override;
    };

  }

}