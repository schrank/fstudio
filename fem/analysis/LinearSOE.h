#pragma once

#include "SOE.h"

#include "../../algebra/Matrix.h"

namespace fem
{

  namespace analysis
  {

    class LinearSOE final:
      virtual public SOE
    {
    public:
      LinearSOE();
      virtual ~LinearSOE();

      void FormSOE(std::shared_ptr<femesh::FEMesh> feMesh) override;
    };

  }

}