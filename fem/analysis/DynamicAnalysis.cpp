#include "DynamicAnalysis.h"

#include <memory>

#include "../exports/VTKFEMManager.h"

namespace fem
{

  namespace analysis
  {

    DynamicAnalysis::DynamicAnalysis(std::shared_ptr<AnalysisModel> model, int numSteps /*= 10*/):
      _model(model), _numSteps(numSteps)
    {}

    bool DynamicAnalysis::Analyze()
    {
      std::shared_ptr<exports::VTKFEMManager> writer = std::make_shared<exports::VTKFEMManager>();

      _model->InitState();

      int step = 0;
      while (step < _numSteps)
      {
        _model->Solve();

        _model->SaveState();

        _model->ExportState(writer, "mesh_" + std::to_string(step));

        step++;
      }

      return 0;
    }

  }

}