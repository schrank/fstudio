#pragma once

#include <memory>

#include "AnalysisModel.h"

namespace fem
{

  namespace analysis
  {

    class DynamicAnalysis
    {
    public:
      DynamicAnalysis() : _model(nullptr), _numSteps(0) {}
      DynamicAnalysis(std::shared_ptr<AnalysisModel> model, int numSteps = 10);
      virtual ~DynamicAnalysis() {}

      virtual bool Analyze();

    private:
      std::shared_ptr<AnalysisModel> _model;
      std::shared_ptr<exports::VTKFEMManager> _writer;
      
      int _numSteps;
    };
  }

}