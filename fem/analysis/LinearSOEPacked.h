#pragma once

#include "SOE.h"

namespace fem
{

  namespace analysis
  {

    class LinearSOEPacked final:
      virtual public SOE
    {
    public:
      LinearSOEPacked();
      virtual ~LinearSOEPacked();

      void FormSOE(std::shared_ptr<femesh::FEMesh> feMesh) override;
    };

  }

}