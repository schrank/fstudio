#include "DOFGroup.h"

namespace fem
{

  namespace analysis
  {

    DOFGroup::DOFGroup(): _tag(0) {}

    DOFGroup::DOFGroup(std::vector<unsigned int> eqNumbers):
      _tag(0),
      _eqNumbers(eqNumbers)
    {}

    DOFGroup::DOFGroup(unsigned int tag, std::vector<unsigned int> eqNumbers):
      _tag(tag),
      _eqNumbers(eqNumbers)
    {}



    DOFGroup::~DOFGroup()
    {

    }

  }

}