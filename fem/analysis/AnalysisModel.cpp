#include "AnalysisModel.h"

#include "DOFGroup.h"
#include "../mesh/FEMNode.h"

#include "../exports/VTKFEMManager.h"

namespace fem
{

  namespace analysis
  {

    AnalysisModel::AnalysisModel() {}

    AnalysisModel::AnalysisModel(
      std::shared_ptr<femesh::FEMesh> femesh,
      std::shared_ptr<Integrator> integrator,
      std::shared_ptr<SOE> soe,
      std::shared_ptr<solver::DynamicSolver> solver) :
        _femesh(femesh), _integrator(integrator), _soe(soe), _solver(solver)
    {}

    AnalysisModel::~AnalysisModel() {}

    void AnalysisModel::Solve()
    {
      _solver->Solve(_soe);

      updateState();
    }

    void AnalysisModel::InitState()
    {
      createEquationNumbers();

      _integrator->FormStiffnessMatrix(_femesh);
      _integrator->FormMassMatrix(_femesh);

      _integrator->FormExternalForceVector(_femesh);
      _integrator->FormResistingForceVector(_femesh);

      _soe->FormSOE(_femesh);
    }

    void AnalysisModel::ExportState(std::shared_ptr<exports::VTKFEMManager> writer, std::string fileName)
    {
      writer->Export(_femesh, fileName);
    }

    void AnalysisModel::SaveState()
    {
       // save all data
    }

    void AnalysisModel::createEquationNumbers()
    {
      long eqnumber = 0;
      for (mesh::NodePtr node : _femesh->GetNodes())
      {
        std::vector<unsigned int> eqNumbers;

        for (unsigned int dof = 0; dof < NDOF; dof++)
          eqNumbers.push_back(eqnumber++);

        FEMNodePtr femnode = std::dynamic_pointer_cast<femesh::FEMNode>(node);
        if (femnode)
          femnode->SetDOFGroup(std::make_shared<DOFGroup>(eqNumbers));
      }
    }

    void AnalysisModel::updateState()
    {
      // update
    }

  }

}