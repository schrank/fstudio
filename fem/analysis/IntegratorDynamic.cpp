#include "IntegratorDynamic.h"

namespace fem
{

  namespace analysis
  {

    void IntegratorDynamic::CommitState()
    {

    }

    void IntegratorDynamic::UpdateState()
    {

    }

    void IntegratorDynamic::FormResistingForceVector(std::shared_ptr<femesh::FEMesh> feMesh)
    {
      feMesh->FormResistingForceVector();
    }

    void IntegratorDynamic::FormMassMatrix(std::shared_ptr<femesh::FEMesh> feMesh)
    {
      feMesh->FormMassMatrix();
    }

    void IntegratorDynamic::FormExternalForceVector(std::shared_ptr<femesh::FEMesh> feMesh)
    {
      feMesh->FormExternalForceVector();
    }

    void IntegratorDynamic::FormStiffnessMatrix(std::shared_ptr<femesh::FEMesh> feMesh)
    {
      feMesh->FormStiffnessMatrix();

      //for (::mesh::PrimitivePtr cell : femesh->GetCells())
      //{
      //   std::shared_ptr<fem::mesh::FEMElement> fe = std::dynamic_pointer_cast<>

      //   fe->ComputeStiffnessAndMassMatrix(
      //      handle<Prototype_Density>(
      //        fe->get_ElementContainer()->getGeometricElement()->getProperty(Prototype_Density_Type())
      //        )->getDensity());

      //  if (options.typeOfDynamicSolver == Newmark)
      //  {
      //    AlgebraUSymMatrix* local_matrix = fe->FormDynamicLocalMatrix(delta_t);
      //    IntVector equations;
      //    fe->get_EquationNumbers(equations);

      //    m_LinearSOE->AddToA(local_matrix, &equations);

      //    delete local_matrix;
      //  }

      //  if ((i + 1) % 1000 == 0)
      //    cout << "  " << i + 1 << " element matrix added: " << System::Clock() - t << endl;
      //  }

      //  cout << "  " << numberofelements << " element matrix added: " << System::Clock() - t << endl;

        //delete[] Elements;
      //}

    }

  }

}