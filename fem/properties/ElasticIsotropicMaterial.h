#pragma once

#include "FEMaterial.h"

namespace fem
{
  namespace prop
  {

    class ElasticIsotropicMaterial:
      virtual public FEMaterial
    {
    public:
      ElasticIsotropicMaterial() {}
      virtual ~ElasticIsotropicMaterial() {}

      //! compute and return elasticity modulus matrix
      void FormDMatrix() override;

      //! compute stress vector from strain vector
      void FormStressVector(std::shared_ptr<algebra::Vector> strain, double detF = 0) override;
    };

  }

}