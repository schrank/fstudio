#pragma once

#include "../../mesh/properties/Property.h"

#include <map>

namespace fem
{
  namespace prop
  {

    class SPConstraint :
      virtual public ::mesh::prop::Property
    {
    public:
      SPConstraint() : Property("SPConstraint"), _sp{} {}
      SPConstraint(std::map<int, double> sp) : Property("SPConstraint"), _sp(sp) {}
      virtual ~SPConstraint() {}

      std::map<int, double> GetConstraint() const {
        return _sp;
      }

      int GetSize() const;

      std::string GetData() const override;

    private:
      std::map<int, double> _sp;
    };

  }
}
