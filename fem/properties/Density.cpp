#include "Density.h"

namespace fem
{
  namespace prop
  {
    double Density::GetDensity() const
    {
      return _density;
    }

    std::string Density::GetData() const
    {
      return std::to_string(_density);
    }
  }
}