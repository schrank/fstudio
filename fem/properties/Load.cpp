#include "Load.h"

namespace fem
{

  namespace prop
  {

    algebra::Vec3 Load::GetLoad() const
    {
      return _load;
    }

    std::string Load::GetData() const
    {
      return std::to_string(_load.X()) + " " + std::to_string(_load.Y()) + " " + std::to_string(_load.Z());
    }
  }

}