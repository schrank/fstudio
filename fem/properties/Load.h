#pragma once

#include "../../algebra/Vec3.h"
#include "../../mesh/properties/Property.h"

namespace fem
{
  namespace prop
  {

    class Load :
      virtual public ::mesh::prop::Property
    {
    public:
      Load() : Property("Load"), _load(algebra::Vec3(0.0, 0.0, 0.0)) {}
      Load(algebra::Vec3 load) : Property("Load"), _load(load) {}
      virtual ~Load() {}

      algebra::Vec3 GetLoad() const;

      std::string GetData() const override;

    private:
      algebra::Vec3 _load;
    };

  }
}
