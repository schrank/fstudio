#pragma once

#include "../../mesh/properties/Property.h"

namespace fem
{
  namespace prop
  {

    class Density :
      virtual public ::mesh::prop::Property
    {
    public:
      Density() : Property("Density"), _density(1) {}
      Density(double density) : Property("Density"), _density(density) {}
      virtual ~Density() {}

      double GetDensity() const;

      std::string GetData() const override;

    private:
      double _density;
    };

  }
}



