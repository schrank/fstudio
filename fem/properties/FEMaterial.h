#pragma once

#include "../../algebra/BaseMatrix.h"
#include "../../algebra/Vector.h"
#include "../../mesh/properties/Property.h"

namespace fem
{
  namespace prop
  {

    class FEMaterial :
      virtual public ::mesh::prop::Property
    {
    public:
      FEMaterial():
        Property("Material"),
        _E(1.0), _nu(0.0),
        _D(nullptr),
        _S(nullptr) {}
      
      FEMaterial(double E, double nu) :
        Property("Material"),
        _E(E), _nu(nu),
        _D(nullptr),
        _S(nullptr) {}
      
      virtual ~FEMaterial() {}

      double GetE() const {
        return _E;
      }

      double GetNu() const {
        return _nu;
      }

      std::string GetData() const override {
        return std::to_string(_E) + " " + std::to_string(_nu);
      }

      //! compute and return elasticity modulus matrix
      virtual void FormDMatrix() = 0;
      //! compute stress vector from strain vector
      virtual void FormStressVector(std::shared_ptr<algebra::Vector> strain, double detF = 0) = 0;

      std::shared_ptr<algebra::BaseMatrix> GetDMatrix() const {
        return _D;
      }

      std::shared_ptr<algebra::Vector> GetStressVector() const {
        return _S;
      }

    protected:
      double _E;
      double _nu;

      // 6x6
      std::shared_ptr<algebra::BaseMatrix> _D;
      // 6
      std::shared_ptr<algebra::Vector> _S;
    };

  }
}