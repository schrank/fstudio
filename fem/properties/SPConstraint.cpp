#include "SPConstraint.h"

namespace fem
{
  namespace prop
  {

    int SPConstraint::GetSize() const
    {
      return _sp.size();
    }

    std::string SPConstraint::GetData() const
    {
      std::string data = "";
      for (std::map<int, double>::const_iterator ci = _sp.begin(); ci != _sp.end(); ci++)
        data += std::to_string(ci->first) + ": " + std::to_string(ci->second) + " ";

      return data;
    }

  }
}