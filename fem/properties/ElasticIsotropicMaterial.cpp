#include "ElasticIsotropicMaterial.h"

namespace fem
{
  namespace prop
  {

    void ElasticIsotropicMaterial::FormDMatrix()
    {
      double lam = _E * _nu / ((1.0 + _nu) * (1.0 - 2.0 * _nu));
      double mu = _E / (2.0 * (1.0 + _nu));
      double mu2 = 2.0 * mu + lam;

      (*_D)(0, 0) = mu2; (*_D)(0, 1) = lam; (*_D)(0, 2) = lam;
      (*_D)(1, 1) = mu2; (*_D)(1, 2) = lam;
      (*_D)(2, 2) = mu2;
      (*_D)(3, 3) = mu;
      (*_D)(4, 4) = mu;
      (*_D)(5, 5) = mu;
    }

    void ElasticIsotropicMaterial::FormStressVector(std::shared_ptr<algebra::Vector> strain, double detF /*= 0*/)
    {
      double c1 = _E * (1 - _nu) / (2 * (1 + _nu) * (1 - 2 * _nu));
      double c2 = c1 * _nu / (1 - _nu);
      double c3 = c1 + 2 * c2;
      double mu = _E / (2 * (1 + _nu));

      double eps0 = (*strain)(0);
      double eps1 = (*strain)(1);
      double eps2 = (*strain)(2);

      // 2nd Piola-Kirchhoff stress
      (*_S)(0) = c1 * eps0 + c2 * (eps1 + eps2) - c3;
      (*_S)(1) = c1 * eps1 + c2 * (eps0 + eps2) - c3;
      (*_S)(2) = c1 * eps2 + c2 * (eps0 + eps1) - c3;
      (*_S)(3) = mu * (*strain)(3);
      (*_S)(4) = mu * (*strain)(4);
      (*_S)(5) = mu * (*strain)(5);
    }

  }
}