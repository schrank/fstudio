#include "NewmarkSolver.h"

namespace fem
{

	namespace solver
	{
		NewmarkSolver::NewmarkSolver(double alpha, double delta):
			DynamicSolver(),
      _c( {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0} ),
			_MassMatrix(nullptr),
			_TangentMatrix(nullptr)
		{}


    NewmarkSolver::NewmarkSolver(
			std::shared_ptr<ls::SOESolver> solver,
			double dt, double alpha /*= 0.25*/, double delta /*= 0.5*/):
			DynamicSolver(solver, dt),
      _c({ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }),
      _MassMatrix(nullptr),
      _TangentMatrix(nullptr)
    {
			formConstants(alpha, delta);
    }

    void NewmarkSolver::Solve(std::shared_ptr<analysis::SOE> soe)
		{
			//const int numequs = soe->;

			//if (!_U)
			//	_U = new AlgebraVector(numequs);

			//if (!_oldU)
			//	_oldU = new AlgebraVector(numequs);

			//if (!_dotU)
			//	_dotU = new AlgebraVector(numequs);

			//if (!_olddotU)
			//	_olddotU = new AlgebraVector(numequs);

			//if (!_ddotU)
			//	_ddotU = new AlgebraVector(numequs);

			//if (!_oldddotU)
			//	_oldddotU = new AlgebraVector(numequs);
			//// ��������� ������ ����������� ��� ��������� � ����������� ���� �������
			//AlgebraVector* tmp = new AlgebraVector(numequs);

			//*tmp = *_u * _c[0] + *_v * _c[2] + *_c * _c[3];

			//AlgebraVector* M = new AlgebraVector;
			//if (options.typeOfMassMatrix == full)
			//	M = RMatVecProduct(_MassMatrix, tmp);
			//else
			//{
			//	M->Resize(numequs);
			//	for (int i = 0; i < numequs; i++)
			//		(*M)(i) = (*_MassMatrix)(i, i) * (*tmp)(i);
			//}

			//*M += *m_LinearSOE->B();
			//delete tmp;

			//cout << "           R = " << M->Norm() << endl << endl;
			//cout << "      Compute displacement" << endl;
			//*_U = _solver->Solve(_TangentMatrix, M);
			//cout << "        Displacement: " << _U->Norm() << endl;
			//delete M;

			//cout << "      Compute acceleration" << endl;
			//*_ddotU = (*_U - *_oldU) * _c[0] - (*_olddotU) * _c[2] - (*_oldddotU) * _c[3];
			//cout << "        Acceleration: " << _ddotU->Norm() << endl;

			//cout << "      Compute velocity" << endl;
			//*_dotU = (*_olddotU) + (*_oldddotU) * _c[6] + (*_ddotU) * _c[7];
			//cout << "        Velocity: " << _dotU->Norm() << endl;

			//m_LinearSOE->set_Acceleration(_ddotU);
			//m_LinearSOE->set_Velocity(_dotU);
			//m_LinearSOE->X(_U);
		}


    void NewmarkSolver::SetCoefficients(double alpha, double delta)
    {
			formConstants(alpha, delta);
    }

		void NewmarkSolver::formConstants(double alpha, double delta)
		{
			//double dt = m_LinearSOE->get_Integrator()->getDeltaT();

			//_c[0] = 1.0 / (alpha * dt * dt);
			//_c[1] = delta / (alpha * dt);
			//_c[2] = 1.0 / (alpha * dt);
			//_c[3] = 1.0 / (2 * alpha) - 1.0;
			//_c[4] = delta / alpha - 1.0;
			//_c[5] = dt * 0.5 * (delta / alpha - 2.0);
			//_c[6] = dt * (1 - delta);
			//_c[7] = dt * delta;
		}

	}
}