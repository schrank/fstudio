#include "DynamicSolver.h"

namespace fem
{
	namespace solver
	{

    void DynamicSolver::CommitState()
    {
      *_u = *_U; *_v = *_V; *_a = *_A;
    }

  }
}