#pragma once

#include "../../algebra/Vector.h"
#include "../../linearSolver/SOESolver.h"
#include "../analysis/SOE.h"

namespace fem
{
  namespace solver
  {
     
		class DynamicSolver
		{
		public:
      DynamicSolver() :
        _solver(nullptr), _dt(0.0),
        _A(nullptr), _U(nullptr), _V(nullptr),
        _a(nullptr), _u(nullptr), _v(nullptr) {}

			DynamicSolver(std::shared_ptr<ls::SOESolver> solver, double dt) :
				_solver(solver), _dt(dt),
				_A(nullptr), _U(nullptr), _V(nullptr),
				_a(nullptr), _u(nullptr), _v(nullptr) {}

			virtual ~DynamicSolver() {}

			//virtual void ComputeVelocity() = 0;
			//virtual void ComputeAcceleration() = 0;
			virtual void Solve(std::shared_ptr<analysis::SOE> soe) = 0;

      const algebra::Vector* CurrentU() const { return _U.get(); }
      const algebra::Vector* CurrentV() const { return _V.get(); }
      const algebra::Vector* CurrentA() const { return _A.get(); }

			void CommitState();

		protected:
      std::shared_ptr<ls::SOESolver> _solver;

			double _dt;

			// current
			std::shared_ptr<algebra::Vector> _A;
			std::shared_ptr<algebra::Vector> _U;
			std::shared_ptr<algebra::Vector> _V;
			// old
			std::shared_ptr<algebra::Vector> _a;
			std::shared_ptr<algebra::Vector> _u;
			std::shared_ptr<algebra::Vector> _v;
		};

  }
}
