#pragma once

#include "DynamicSolver.h"

#include <array>

#include "../../algebra/RowPackedMatrix.h"

namespace fem
{
  namespace solver
  {

		class NewmarkSolver final:
			public virtual DynamicSolver
		{
		public:
			NewmarkSolver(double alpha = 0.25, double delta = 0.5);
			NewmarkSolver(std::shared_ptr<ls::SOESolver> solver, double dt, double alpha = 0.25, double delta = 0.5);

			virtual ~NewmarkSolver() {}

			void Solve(std::shared_ptr<analysis::SOE> soe) override;

			void SetCoefficients(double alpha, double delta);

		private:
      void formConstants(double alpha, double delta);

		private:
			std::array<double, 8> _c;

			std::shared_ptr<algebra::RowPackedMatrix> _MassMatrix;
			std::shared_ptr<algebra::RowPackedMatrix> _TangentMatrix;
		};

  }
}