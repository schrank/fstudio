#pragma once

#include <iostream>
#include <string>
#include <map>

static std::map<std::string, unsigned int> PropertyField =
{
  {"Density", 1},
  {"Material", 2},
  {"SPConstraint", 1}
};

static std::string EmptyData(std::string name) {
  std::string result = "";
  for (unsigned int i = 0; i < PropertyField[name]; i++)
    result += "-1";

  return result + " ";
}

namespace mesh
{

  namespace prop
  {
    class Property
    {
    public:
      Property();
      Property(std::string name);
      ~Property();

      virtual std::string GetData() const = 0;

      std::string GetName() const;

      friend std::ostream& operator<<(std::ostream& out, const Property& rhs);

    private:
      std::string _name;
    };

  }
}


