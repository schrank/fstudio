#include "Property.h"

namespace mesh
{
  namespace prop
  {

    Property::Property() : _name("Property") {}
    Property::Property(std::string name) : _name(name) {}
    Property::~Property() {}

    std::string Property::GetName() const
    {
      return _name;
    }

    std::string Property::GetData() const
    {
      return "";
    }

    std::ostream& operator<<(std::ostream& out, const Property& rhs)
    {
      out << rhs.GetName() << " " << rhs.GetData() << std::endl;

      return out;
    }
  }
}