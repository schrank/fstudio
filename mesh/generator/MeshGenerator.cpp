#include "MeshGenerator.h"

#include "../mesh/Mesh.h"
#include "../mesh/Node.h"

namespace mesh
{
  
  namespace generator
  {
    void MeshGenerator::CreateBlock(
      std::shared_ptr<Mesh> mesh,
      double Lx, double Ly, double Lz,
      unsigned nx, unsigned ny, unsigned nz)
    {
      for (unsigned int i = 0; i < nx; i++)
        for (unsigned int j = 0; j < ny; j++)
          for (unsigned int k = 0; k < nz; k++)
            mesh->AddNode(std::make_shared<Node>(Lx * i / nx, Ly * j / ny, Lz * k / nz));

      mesh->AddHexahedron(
        mesh->GetNode(0), mesh->GetNode(1), mesh->GetNode(2), mesh->GetNode(3),
        mesh->GetNode(5), mesh->GetNode(6), mesh->GetNode(4), mesh->GetNode(7));
    }

  }
}