#pragma once

#include "../mesh/Mesh.h"

namespace mesh
{

  namespace generator
  {
    class MeshGenerator
    {
    public:
      MeshGenerator() {}
      ~MeshGenerator() {}

      void CreateBlock(std::shared_ptr<Mesh> mesh,
        double Lx, double Ly, double Lz,
        unsigned nx, unsigned ny, unsigned nz);
    };
  }
}

