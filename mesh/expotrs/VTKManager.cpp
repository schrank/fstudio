#include "VTKManager.h"

#include <fstream>
#include <iostream>
#include <map>
#include <stdexcept>

#include "../mesh/Primitive.h"
#include "../mesh/Mesh.h"
#include "../mesh/Node.h"

namespace mesh
{
	namespace exports
	{
		std::map<std::string, unsigned int> CellType = 
		{
			{"Node", 1},
			{"Edge", 3},
			{"Triangle", 5},
			{"Tetrahedron", 10},
			{"Hexahedron", 11 }
		};

		void VTKManager::Export(std::shared_ptr<Mesh> mesh, std::string filename)
		{
			std::ofstream outfile(filename.c_str());

			if (outfile.is_open())
			{
				std::stringstream out;

				header(out, mesh);
				outfile << out.rdbuf();
				out.str("");

				points(out, mesh);
				outfile << out.rdbuf();
				out.str("");

				cells(out, mesh);
				outfile << out.rdbuf();
				out.str("");

				footer(out, mesh);
				outfile << out.rdbuf();
				out.str("");

				outfile.close();
			}
			else
				throw std::logic_error("VTKManager::Export: Error with mesh file!");
		}

		void VTKManager::dataArrayNode(
			std::stringstream& o,
			std::string params,
			std::string type,
			std::string name,
			unsigned int noc) const
		{
			if (!params.empty())
			{
				std::string result = "        <DataArray type=\"" + type + "\" Name=\"" + name + "\" ";

				if (noc > 0)
					result += "NumberOfComponents=\"" + std::to_string(noc) + "\" ";

				result += "Format=\"ascii\">\n";

				o << result << params + "\n" << "        </DataArray>\n";
			}
		}
		
		void VTKManager::header(std::stringstream& out, std::shared_ptr<Mesh> mesh) const
		{
			out << "<?xml version=\"1.0\"?>\n";
			out << "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n";
			out << "  <UnstructuredGrid>\n";
			out << "    <Piece NumberOfPoints=\"" << mesh->GetNumberOfNodes()
				<< "\" NumberOfCells=\"" << mesh->GetNumberOfCells() << "\">\n";
		}

		void VTKManager::points(std::stringstream& out, std::shared_ptr<Mesh> mesh) const
		{
			std::string coordinates = "";
			for (NodePtr node : mesh->GetNodes())
				coordinates +=  std::to_string(node->X()) + " "
										  + std::to_string(node->Y()) + " "
											+ std::to_string(node->Z()) + " ";

			out << "      <Points>\n";
			dataArrayNode(out, coordinates, "Float64", "coordinates", 3);
			out << "      </Points>\n";
		}

		void VTKManager::cells(std::stringstream& out, std::shared_ptr<Mesh> mesh) const
		{
			std::string connectivity = "";
			std::string offsets = "";
			std::string types = "";
			unsigned int offset = 0;

			for (PrimitivePtr cell : mesh->GetCells())
			{
				offset += cell->NumberOfNodes();
				offsets += std::to_string(offset) + " ";

				types += std::to_string(CellType[cell->GetName()]) + " ";

				for (NodePtr node : cell->GetNodes())
					connectivity += std::to_string(mesh->FindNode(node)) + " ";

				out << "      <Cells>\n";
				dataArrayNode(out, connectivity, "Int64", "connectivity");
				dataArrayNode(out, offsets, "Int64", "offsets");
				dataArrayNode(out, types, "Int64", "types");
				out << "      </Cells>\n";
			}
		}

		void VTKManager::footer(std::stringstream& out, std::shared_ptr<Mesh> mesh) const
		{
			out << "    </Piece>\n";
			out << "</UnstructuredGrid>\n";
			out << "</VTKFile>";
		}
	}
}