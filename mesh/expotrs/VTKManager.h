#pragma once

#include <string>
#include <sstream>

namespace mesh
{

  class Mesh;

  namespace exports
  {

    class VTKManager
    {
    public:
      VTKManager() {}

      void Export(std::shared_ptr<Mesh>, std::string filename);

      virtual ~VTKManager() {}

    protected:
      void cells(std::stringstream& out, std::shared_ptr<Mesh>) const;
      void footer(std::stringstream& out, std::shared_ptr<Mesh>) const;
      void header(std::stringstream& out, std::shared_ptr<Mesh>) const;
      void points(std::stringstream& out, std::shared_ptr<Mesh>) const;

      void dataArrayNode(
        std::stringstream& o,
        std::string params,
        std::string type,
        std::string name, unsigned int noc = 0) const;
    };

  }
}