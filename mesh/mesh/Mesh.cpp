#include "Mesh.h"

#include "Hexahedron.h"

namespace mesh
{
  Mesh::Mesh() {}

  Mesh::~Mesh()
  {
    _nodes.clear();

    _cells.clear();
  }

  unsigned int Mesh::FindNode(NodePtr node) const
  {
    return static_cast<unsigned int>
      (std::distance(_nodes.begin(), find(_nodes.begin(), _nodes.end(), node)));
  }

  void Mesh::AddNode(NodePtr node)
  {
    _nodes.push_back(node);
  }

  unsigned int Mesh::GetNumberOfNodes() const {
    return static_cast<unsigned int>(_nodes.size());
  }

  unsigned int Mesh::GetNumberOfCells() const {
    return static_cast<unsigned int>(_cells.size());
  }

  NodePtr Mesh::GetNode(unsigned int index) const {
    return _nodes[index];
  }

  PrimitivePtr Mesh::GetCell(unsigned int index) const {
    return _cells[index];
  }

  std::vector<NodePtr> Mesh::GetNodes()
  {
    return _nodes;
  }

  std::vector<NodePtr> Mesh::GetNodes() const
  {
    return _nodes;
  }

  std::vector<PrimitivePtr> Mesh::GetCells() const
  {
    return _cells;
  }

  std::vector<PrimitivePtr>& Mesh::GetCells()
  {
    return _cells;
  }

  void Mesh::AddHexahedron(NodePtr n1, NodePtr n2, NodePtr n3, NodePtr n4, NodePtr n5, NodePtr n6, NodePtr n7, NodePtr n8)
  {
    _cells.push_back(std::make_shared<Hexahedron>(std::vector<NodePtr>{ n1, n2, n3, n4, n5, n6, n7, n8 }));
  }

  std::ostream& operator<<(std::ostream& out, const Mesh& rhs)
  {
    out << "Nodes\n\n";
    int index = 0;
    for (mesh::NodePtr node : rhs.GetNodes())
      out << index++ << "  " << *node;

    out << std::endl;

    out << "Elements\n\n";

    for (mesh::PrimitivePtr figure : rhs.GetCells())
    {
      out << figure->GetName() << ": ";

      for (mesh::NodePtr node : figure->GetNodes())
        out << rhs.FindNode(node) << " ";

      out << std::endl;

      out << *figure;
    }

    out << std::endl;

    return out;
  }
}