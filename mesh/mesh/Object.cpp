#include "Object.h"

namespace mesh
{
  Object::Object(): _properties({})
  {
    init("Object");
  }
 
  Object::~Object()
  {
    if (_id)
      delete _id;

    _properties.clear();
  }

  std::string Object::GetName() const
  {
    return _name;
  }

  std::vector<PropPtr> Object::GetProperties() const
  {
    return _properties;
  }

  void Object::AddProperty(PropPtr property)
  {
    _properties.push_back(property);
  }

  PropPtr Object::GetProperty(std::string name) const
  {
    for (PropPtr p : _properties)
      if (p->GetName().compare(name) == 0)
        return p;

    return nullptr;
  }

  void Object::init(std::string name)
  {
    _name = name;
    _id = nullptr;
  }
}