#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "Id.h"
#include "../../algebra/Vec3.h"
#include "../properties/Property.h"

namespace mesh
{

  using PropPtr = std::shared_ptr<prop::Property>;

  class Object
  {
  public:
    Object();
    virtual ~Object();

    std::string GetName() const;

    inline Id* Object::GetId() const { return _id; }

    std::vector<PropPtr> GetProperties() const;

    void AddProperty(PropPtr prop);
    PropPtr GetProperty(std::string name) const;

  protected:
    virtual void init(std::string name) = 0;

  protected:
    std::string _name;
    Id* _id;

    std::vector<PropPtr> _properties;
  };

}