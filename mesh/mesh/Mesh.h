#pragma once

#include <iostream>
#include <vector>

#include "Primitive.h"

namespace mesh
{
  class Node;
  class Primitive;

  class Mesh
  {
  public:
    Mesh();
    Mesh(const Mesh& mesh) {}
    virtual ~Mesh();

    void AddNode(NodePtr n);

    void AddHexahedron(NodePtr n1, NodePtr n2, NodePtr n3, NodePtr n4,
                       NodePtr n5, NodePtr n6, NodePtr n7, NodePtr n8);

    unsigned int FindNode(NodePtr node) const;

    unsigned int GetNumberOfNodes() const;

    unsigned int GetNumberOfCells() const;

    std::vector<NodePtr> GetNodes();
    std::vector<NodePtr> GetNodes() const;

    std::vector<PrimitivePtr>& GetCells();
    std::vector<PrimitivePtr> GetCells() const;
    NodePtr GetNode(unsigned int index) const;
    PrimitivePtr GetCell(unsigned int index) const;

    friend std::ostream& operator<<(std::ostream& out, const Mesh& rhs);

  protected:
    std::vector<NodePtr> _nodes;
    std::vector<PrimitivePtr> _cells;
  };

}