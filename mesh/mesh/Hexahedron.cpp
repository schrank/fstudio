#include "Hexahedron.h"

namespace mesh
{
	void swap(NodePtr* rhs, NodePtr* lhs)
	{
		NodePtr tmp = *rhs;
		*rhs = *lhs;
		*lhs = tmp;
	}

  Hexahedron::Hexahedron() : Primitive(std::vector<NodePtr>{})
	{
		init("Hexahedron");
	}

  Hexahedron::Hexahedron(std::vector<NodePtr> nodes) : Primitive(nodes)
	{
		init("Hexahedron");
		changeOrientation();
	}

  Hexahedron::~Hexahedron() {}
  
  void Hexahedron::changeOrientation()
  {
		for (int j = 0; j < _nodes.size(); j++)
			for (int k = 0; k < _nodes.size() - j - 1; k++)
			{
				if (_nodes[k]->Z() > _nodes[k + 1]->Z())
					swap(&_nodes[k], &_nodes[k + 1]);
			}

		for (int j = 0; j < 4; j++)
			for (int k = 0; k < 4 - j - 1; k++)
			{
				if (_nodes[k]->Y() > _nodes[k + 1]->Y())
					swap(&_nodes[k], &_nodes[k + 1]);
			}

		for (int j = 0; j < 4; j++)
			for (int k = 4; k < 8 - j - 1; k++)
			{
				if (_nodes[k]->Y() > _nodes[k + 1]->Y())
					swap(&_nodes[k], &_nodes[k + 1]);
			}

		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 2; j++)
				for (int k = i * 2; k < i * 2 + 2 - j - 1; k++)
				{
					if (_nodes[k]->X() > _nodes[k + 1]->X())
						swap(&_nodes[k], &_nodes[k + 1]);
				}

		for (int i = 0; i < 4; i++)
		{
			if (_nodes[i * 2]->X() > _nodes[i * 2 + 2 - 1]->X())
				swap(&_nodes[i * 2], &_nodes[i * 2 + 2 - 1]);
		}
  }

}