#pragma once

#include <memory>
#include <vector>

#include "Node.h"

namespace mesh
{

  class Primitive:
    virtual public Object
  {
  public:
    Primitive();
    Primitive(std::vector<NodePtr> nodes);
    virtual ~Primitive();

    unsigned int NumberOfNodes() const;

    NodePtr GetNode(int index) const;
    std::vector<NodePtr> GetNodes() const;

    unsigned int FindNode(NodePtr node) const;

    friend std::ostream& operator<<(std::ostream& out, const Primitive& rhs);

  protected:
    void init(std::string name) override;

  protected:
    std::vector<NodePtr> _nodes;
  };

  using PrimitivePtr = std::shared_ptr<Primitive>;

}