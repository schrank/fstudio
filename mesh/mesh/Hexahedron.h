#pragma once

#include "Primitive.h"

namespace mesh
{

  class Hexahedron :
    public virtual Primitive
  {
  public:
    Hexahedron();
    Hexahedron(std::vector<NodePtr> nodes);
    virtual ~Hexahedron();

  protected:
    void changeOrientation();
  };

}