#include "Node.h"

namespace mesh
{
  Node::Node() : _x(0), _y(0), _z(0)
  {
    init("Node");
  }

  Node::Node(double x, double y, double z) : _x(x), _y(y), _z(z)
  {
    init("Node");
  }

  Node::Node(const Node& rhs) : _x(rhs._x), _y(rhs._y), _z(rhs._z)
  {
    init("Node");
  }

  Node& Node::operator=(const Node& rhs)
  {
    _x = rhs.X();
    _y = rhs.Y();
    _z = rhs.Z();

    return *this;
  }

  bool Node::operator==(const Node& rhs)
  {
    return _id == rhs._id;
  }

  algebra::Vec3 Node::XYZ() const
  {
    return algebra::Vec3(_x, _y, _z);
  }

  void Node::init(std::string name)
  {
    _name = name;
    _id = new Id(algebra::Vec3(_x, _y, _z));
  }

  std::ostream& operator<<(std::ostream& out, const Node& rhs)
  {
    out << rhs._x << " " << rhs._y << " " << rhs._z << std::endl;

    for (PropPtr p : rhs._properties)
      out << p->GetName() << " " << p->GetData() << std::endl;

    return out;
  }

}
