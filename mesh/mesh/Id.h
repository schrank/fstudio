#pragma once

#include "../../algebra/Vec3.h"

namespace mesh
{

  class Id
  {
  public:
    Id(): _v(-1e-60, -1e-60, -1e-60) {}
    Id(algebra::Vec3 v): _v(v) {}
    ~Id() {}

    bool operator<(const Id& rhs) const
    {
      return _v < rhs._v;
    }

    bool operator==(const Id& rhs) {
      return _v == rhs._v;
    }

  private:
    algebra::Vec3 _v;
  };

}