#pragma once

#include <iostream>

#include "Object.h"

namespace mesh
{

  class Node:
    virtual public Object
  {
  public:
    Node();
    Node(double x, double y, double z);
    Node(const Node& rhs);

    virtual ~Node() {}

    Node& operator=(const Node& rhs);

    bool operator==(const Node& rhs);

    double X() const { return _x; }

    double Y() const { return _y; }

    double Z() const { return _z; }

    algebra::Vec3 XYZ() const;

    friend std::ostream& operator<<(std::ostream& out, const Node& rhs);

  protected:
   void init(std::string /*name*/) override;

  protected:
    double _x;
    double _y;
    double _z;
  };

  using NodePtr = std::shared_ptr<::mesh::Node>;
}