#include "Primitive.h"

namespace mesh
{
  

  Primitive::Primitive(): _nodes(std::vector<NodePtr>{})
  {
    init("Primitive");
  }

  Primitive::Primitive(std::vector<NodePtr> nodes): _nodes(nodes)
  {
    init("Primitive");
  }

  Primitive::~Primitive()
  {
    _nodes.clear();
  }

  unsigned int Primitive::FindNode(NodePtr node) const {
    return static_cast<unsigned int>(std::distance(_nodes.begin(), std::find(_nodes.begin(), _nodes.end(), node)));
  }

  unsigned int Primitive::NumberOfNodes() const {
    return static_cast<unsigned int>(_nodes.size());
  }

  NodePtr Primitive::GetNode(int index) const
  {
    return _nodes[index];
  }

  std::vector<NodePtr> Primitive::GetNodes() const
  {
    return _nodes;
  }

  void Primitive::init(std::string name)
  {
    _name = name;

    algebra::Vec3 v;

    for (std::vector<NodePtr>::const_iterator it = _nodes.begin(); it != _nodes.end(); it++)
      v += (*it)->XYZ();

    _id = new Id(v / (double) _nodes.size());
  }

  std::ostream& operator<<(std::ostream& out, const Primitive& rhs)
  {
    for (NodePtr node : rhs._nodes)
      out << *node;

    for (PropPtr prop : rhs._properties)
      out << *prop;
 
    return out;
  }
}