#pragma once

namespace options
{

  class Options
  {
  public:
    Options();
    virtual ~Options();

    virtual bool ParseOptions() = 0;
  };

}