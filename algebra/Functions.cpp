#include "Functions.h"

#include <iostream>
#include <stdexcept>



namespace algebra
{

  Matrix& Inverse3(Matrix& A)
  {
    int m = A.Rows();
    int n = A.Cols();

    if ((m != n) || (m != 3) || (n != 3))
      throw std::logic_error("ERROR algebra::Inverse3() - not square matrix!");

    Matrix inverseA(3, 3);

    double M11 = A(1, 1) * A(2, 2) - A(2, 1) * A(1, 2);
    double M12 = A(1, 0) * A(2, 2) - A(2, 0) * A(1, 2);
    double M13 = A(1, 0) * A(2, 1) - A(2, 0) * A(2, 1);
    double M21 = A(0, 1) * A(2, 2) - A(2, 1) * A(0, 2);
    double M22 = A(0, 0) * A(2, 2) - A(2, 0) * A(0, 2);
    double M23 = A(0, 0) * A(2, 1) - A(2, 0) * A(0, 1);
    double M31 = A(0, 1) * A(1, 2) - A(1, 1) * A(0, 2);
    double M32 = A(0, 0) * A(1, 2) - A(1, 0) * A(0, 2);
    double M33 = A(0, 0) * A(1, 1) - A(1, 0) * A(0, 1);

    double det = A(0, 0) * M11 - A(0, 1) * M12 + A(0, 2) * M13;

    //�������� �������
    inverseA(0, 0) = M11 / det; inverseA(0, 1) = -M21 / det; inverseA(0, 2) = M31 / det;
    inverseA(1, 0) = -M12 / det; inverseA(1, 1) = M22 / det; inverseA(1, 2) = -M32 / det;
    inverseA(2, 0) = M13 / det; inverseA(2, 1) = -M23 / det; inverseA(2, 2) = M33 / det;

    return inverseA;
  }

  std::shared_ptr<algebra::Matrix> Inverse3(std::shared_ptr<Matrix> A)
  {
    int m = A->Rows();
    int n = A->Cols();

    if ((m != n) || (m != 3) || (n != 3))
      throw std::logic_error("ERROR algebra::Inverse3() - not square matrix!");

    double M11 = (*A)(1, 1) * (*A)(2, 2) - (*A)(2, 1) * (*A)(1, 2);
    double M12 = (*A)(1, 0) * (*A)(2, 2) - (*A)(2, 0) * (*A)(1, 2);
    double M13 = (*A)(1, 0) * (*A)(2, 1) - (*A)(2, 0) * (*A)(2, 1);
    double M21 = (*A)(0, 1) * (*A)(2, 2) - (*A)(2, 1) * (*A)(0, 2);
    double M22 = (*A)(0, 0) * (*A)(2, 2) - (*A)(2, 0) * (*A)(0, 2);
    double M23 = (*A)(0, 0) * (*A)(2, 1) - (*A)(2, 0) * (*A)(0, 1);
    double M31 = (*A)(0, 1) * (*A)(1, 2) - (*A)(1, 1) * (*A)(0, 2);
    double M32 = (*A)(0, 0) * (*A)(1, 2) - (*A)(1, 0) * (*A)(0, 2);
    double M33 = (*A)(0, 0) * (*A)(1, 1) - (*A)(1, 0) * (*A)(0, 1);

    double det = (*A)(0, 0) * M11 - (*A)(0, 1) * M12 + (*A)(0, 2) * M13;

    //�������� �������
    std::shared_ptr<Matrix> inverseA = std::make_shared<Matrix>(3, 3);

    (*inverseA)(0, 0) = M11 / det; (*inverseA)(0, 1) = -M21 / det; (*inverseA)(0, 2) = M31 / det;
    (*inverseA)(1, 0) = -M12 / det; (*inverseA)(1, 1) = M22 / det; (*inverseA)(1, 2) = -M32 / det;
    (*inverseA)(2, 0) = M13 / det; (*inverseA)(2, 1) = -M23 / det; (*inverseA)(2, 2) = M33 / det;

    return inverseA;
  }

}