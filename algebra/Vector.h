#pragma once

#include <iostream>

#include "Array.h"

namespace algebra
{
  class BaseMatrix;

  typedef algebra::Array<double> DoubleArray;

  class Vector: public virtual DoubleArray
  {
  public:
    Vector() : DoubleArray() {}
    Vector(int size) : DoubleArray(size) {}
    Vector(const Vector& rhs) : DoubleArray(rhs) {};

    virtual ~Vector() {}

    const double operator()(int index) const;
    double& operator()(int index);

    Vector& operator=(const Vector &vector);
    Vector& operator=(const double &value);

    Vector operator+(const Vector &a);
    Vector operator+(const double value);
    Vector& operator+=(const Vector &vector);
    Vector& operator+=(const double);

    Vector operator-() const;
    Vector operator-(const Vector &a);
    Vector operator-(const double value);
    Vector& operator-=(const Vector &vector);
    Vector& operator-=(const double);

    Vector operator*(const double value);
    Vector& operator*=(const double);
    double operator*(const Vector &b);
    Vector operator*(const BaseMatrix &matrix);
    
    Vector& operator/=(const double);
    Vector operator/(const double value);
    
    Vector& operator<=(const Vector &aVector);

    bool operator==(const Vector& rhs) const;
    bool operator!=(const Vector& rhs) const;

    virtual double EnergyNorm();
    virtual double Norm() const;
    virtual void Normalize();

    virtual void Floor();

  protected:
    friend std::ostream& operator<<(std::ostream& Stream, const Vector& rhs);
  };

  std::ostream& operator<<(std::ostream& Stream, const Vector& rhs);
}
