#include "Vector.h"

#include <algorithm>

#include "BaseMatrix.h"

namespace algebra
{
	double& Vector::operator() (int index)
	{
		return (*this)[index];
	}

	const double Vector::operator() (int index) const
	{
		return (*this)[index];
	}

	Vector& Vector::operator= (const Vector& vector)
	{
		if (_arraySize == vector._arraySize)
			for (int i = 0; i < _arraySize; i++)
				_dataPtr[i] = (vector._dataPtr)[i];
		else
			throw std::logic_error("Error: Size mismatch in Vector& Vector::operator= (const Vector &vector)");

		return *this;
	}

	Vector& Vector::operator= (const double& value)
	{
		for (int i = 0; i < _arraySize; i++)
			_dataPtr[i] = value;

		return *this;
	}

	Vector& Vector::operator+= (const Vector& vector)
	{
		if (_arraySize == vector._arraySize)
			for (int i = 0; i < _arraySize; i++)
				_dataPtr[i] += (vector._dataPtr)[i];
		else
			throw std::logic_error("Error: Size mismatch in Vector& Vector::operator+= (const Vector &vector)");

		return *this;
	}

	Vector& Vector::operator+= (const double value)
	{
		for (int i = 0; i < _arraySize; i++)
			_dataPtr[i] += value;

		return *this;
	}

	Vector& Vector::operator-= (const Vector& vector)
	{
		if (_arraySize == vector._arraySize)
			for (int i = 0; i < _arraySize; i++)
				_dataPtr[i] -= (vector._dataPtr)[i];
		else
			throw std::logic_error("Error: Size mismatch in Vector& Vector::operator-= (const Vector &vector)");

		return *this;
	}

	Vector& Vector::operator-= (const double value)
	{
		for (int i = 0; i < _arraySize; i++)
			_dataPtr[i] -= value;

		return *this;
	}

	Vector& Vector::operator*= (const double value)
	{
		for (int i = 0; i < _arraySize; i++)
			_dataPtr[i] *= value;

		return *this;
	}

	Vector& Vector::operator/= (const double value)
	{
		if (value)
			for (int i = 0; i < _arraySize; i++)
				_dataPtr[i] /= value;
		else
			throw std::logic_error("Error: Division by zero in Vector& Vector::operator/= (const Vector &vector)");

		return *this;
	}

	Vector& Vector::operator<= (const Vector& aVector)
	{
		int size = std::min(Size(), aVector.Size());
		for (int i = 0; i < size; i++)
			operator()(i) = aVector(i);

		return *this;
	}

	double Vector::Norm() const
	{
		double norm = 0;

		for (int i = 0; i < _arraySize; i++)
			norm += _dataPtr[i] * _dataPtr[i];

		return sqrt(norm);
	}

	void Vector::Normalize()
	{
		double norm = Norm();
		int size = Size();
		if (norm)
			for (int i = 0; i < size; i++)
				_dataPtr[i] /= norm;
	}

	Vector Vector::operator+ (const Vector& a)
	{
		Vector result(*this);
		result += a;

		return result;
	}

	Vector Vector::operator+ (const double value)
	{
		Vector result(*this);
		result += value;

		return result;
	}

	Vector Vector::operator- (const Vector& a)
	{
		Vector result(*this);
		result -= a;

		return result;
	}

	Vector Vector::operator- (const double value)
	{
		Vector result(*this);
		result -= value;

		return result;
	}

	Vector Vector::operator* (const double value)
	{
		Vector result(*this);
		result *= value;

		return result;
	}

	double Vector::operator* (const Vector& b)
	{
		int bsize = b.Size();
		if (Size() == bsize)
		{
			double result = 0;
			for (int i = 0; i < bsize; i++)
				result += (*this)(i) * b(i);

			return result;
		}
		else
			throw std::logic_error("Error: Vector::operator* (Vector &) : Sizes Mismatch");
	}

	Vector Vector::operator* (const BaseMatrix& matrix)
	{
		int rows = matrix.Rows();
		int cols = matrix.Cols();
		if (Size() == rows)
		{
			Vector result(cols);
			for (int i = 0; i < cols; i++)
			{
				double sum = 0;
				for (int j = 0; j < rows; j++)
					sum += _dataPtr[j] * matrix(j, i);

				result(i) = sum;
			}
			return result;
		}
		else
			throw std::logic_error("Error: Vector::operator* (AlgebraMatrix &) : Sizes Mismatch");
	}

	Vector Vector::operator/ (const double value)
	{
		if (value)
		{
			Vector result(*this);
			result /= value;
			return result;
		}
		else
			throw std::logic_error("Error: Vector::operator/ (double) : Divide by Zero");
	}

	bool Vector::operator==(const Vector& rhs) const
	{
		bool flag = true;
		if (Size() == rhs.Size())
			for (int i = 0; i < Size(); i++)
				if ((*this)(i) != rhs(i))
				{
					flag = false;
					break;
				}
				else
					flag = false;

		return flag;
	}

	bool Vector::operator!=(const Vector& rhs) const
	{
		return !operator==(rhs);
	}

	Vector Vector::operator-() const
	{
		Vector res(Size());
		for (int i = 0; i < Size(); i++)
			res(i) = -(*this)(i);

		return res;
	}

	void Vector::Floor()
	{
		for (int i = 0; i < Size(); i++)
			(*this)(i) = floor(_dataPtr[i] * 1e18) / 1e18;
	}

	double Vector::EnergyNorm()
	{
		double r = 0.0;

		for (int i = 0; i < Size(); i++)
			if (fabs(_dataPtr[i]) > r)
				r = fabs(_dataPtr[i]);

		return r;
	}

	std::ostream& operator<<(std::ostream& Stream, const Vector& rhs)
	{
		Stream.flags(std::ios::scientific | std::ios::left | std::ios::dec);
		Stream.precision(7);

		Stream << "(";

		int size = rhs.Size();
		for (int i = 0; i < size; i++)
		{
			Stream << rhs(i);
			if (i != size - 1)
				Stream << ", ";
		}
		Stream << ")" << std::endl;

		return Stream;
	}

}