#pragma once

#include <iostream>

#include "BaseMatrix.h"
#include "Series.h"

namespace algebra
{
  class Vector;

  class RowPackedMatrix final:
    virtual public BaseMatrix
  {
  public:
    RowPackedMatrix();
    RowPackedMatrix(int rows, int cols);
    RowPackedMatrix(const RowPackedMatrix& RowPackedMatrix);
    ~RowPackedMatrix();

    void AddItem(int row, int col, double value);
    void AddNewItem(int row, int col, double value);
    void Resize(int nrows, int ncols) override;
    void UpdateCol(int col, Vector v);
    void Zero();
    void ZeroCol(int col) override;
    void ZeroRow(int row) override;

    double& operator() (int row, int col) override;
    const double& operator() (int row, int col) const override;

    RowPackedMatrix& operator= (RowPackedMatrix& RowPackedMatrix);
    RowPackedMatrix& operator*= (const double value);
    RowPackedMatrix& operator*= (const double value) const override;
    RowPackedMatrix& operator/= (const double value);
    RowPackedMatrix operator/ (const double value);

    double* DataArray() const override;

    algebra::Vector Col(int Col) const;
    algebra::Vector Row(int Row) const;

    Series& RowData(int row);
    const Series& RowData(int row) const ;

    double Determinant() const override;
    double SumRowElements(int Row) const;

  protected:
    friend std::ostream& operator<<(std::ostream& Stream, const algebra::RowPackedMatrix& rhs);

  protected:
    long _size;

    static double zero;

    Series* _data;
  };

  std::ostream& operator<<(std::ostream& Stream, const algebra::RowPackedMatrix& rhs);

}