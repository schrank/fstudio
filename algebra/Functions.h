#pragma once

#include <memory>

#include "Matrix.h"

namespace algebra
{

  Matrix& Inverse3(Matrix& A);

  std::shared_ptr<Matrix> Inverse3(std::shared_ptr<Matrix> A);

}