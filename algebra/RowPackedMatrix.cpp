#include "RowPackedMatrix.h"

#include <algorithm>
#include <fstream>

#include "Vector.h"

using namespace std;

namespace algebra
{

  double RowPackedMatrix::zero = 1e-16;

  RowPackedMatrix::RowPackedMatrix() : BaseMatrix(), _size(0), _data(nullptr) {}

  RowPackedMatrix::RowPackedMatrix(int rows, int cols) : BaseMatrix(rows, cols)
  {
    _size = _rows * _cols;

    if (_size > 0)
    {
      _data = new Series[_rows];

      if (!_data)
        throw std::logic_error("RowPackedMatrix: No Memory");
    }
  }

  RowPackedMatrix::RowPackedMatrix(const RowPackedMatrix& M) :
    _data(NULL)
  {
    _rows = M.Rows();
    _cols = M.Cols();

    _size = _rows * _cols;

    _data = new Series[_rows];

    if (!_data)
      throw std::logic_error("RowPackedMatrix: No Memory");

    for (int i = 0; i < _rows; i++)
      _data[i] = M.RowData(i);
  }

  RowPackedMatrix::~RowPackedMatrix(void)
  {
    if (_data)
      delete[] _data;
  }

  void RowPackedMatrix::AddItem(int row, int col, double value)
  {
    if (value)
      _data[row].AddItem(col, value);
  }

  void RowPackedMatrix::AddNewItem(int row, int col, double value)
  {
    if (value)
      _data[row].AddNewItem(col, value);
  }

  void RowPackedMatrix::ZeroCol(int col)
  {
    for (int i = 0; i < _rows; i++)
      _data[i].RemoveItem(col);
  }

  void RowPackedMatrix::ZeroRow(int row)
  {
    _data[row].Clear();
  }

  void RowPackedMatrix::UpdateCol(int col, Vector v)
  {
    if (col < _cols)
      for (int i = 0; i < _rows; i++)
        _data[i].UpdateItem(col, v(i));
  }


  double& RowPackedMatrix::operator() (int row, int col)
  {
    int ind = _data[row].Find(col);

    if (ind != -1)
      return _data[row](ind);
    else
      return zero = 0.0;
  }

  const double& RowPackedMatrix::operator() (int row, int col) const
  {
    int ind = _data[row].Find(col);

    if (ind != -1)
      return _data[row](ind);
    else
      return zero = 0.0;
  }

  RowPackedMatrix& RowPackedMatrix::operator= (RowPackedMatrix& matrix)
  {
    int row = matrix.Rows();
    int col = matrix.Cols();

    if ((row == _rows) && (col == _cols))
      for (int i = 0; i < _rows; i++)
        _data[i] = matrix.RowData(i);

    return *this;
  }

  RowPackedMatrix& RowPackedMatrix::operator*= (const double value)
  {
    for (int i = 0; i < _rows; i++)
      for (int j = 0; j < _data[i].RealSize(); j++)
        _data[i](j) *= value;

    return *this;
  }

RowPackedMatrix& RowPackedMatrix::operator*=(const double value) const
  {
    RowPackedMatrix tmp(*this);

    for (int i = 0; i < _rows; i++)
      for (int j = 0; j < _data[i].RealSize(); j++)
        tmp._data[i](j) *= value;

    return tmp;
  }

  RowPackedMatrix& RowPackedMatrix::operator/= (const double value)
  {
    if (value)
      for (int i = 0; i < _rows; i++)
        for (int j = 0; j < _data[i].RealSize(); j++)
          _data[i](j) *= value;
    else
      throw std::logic_error("Zero division in RowPackedMatrix& RowPackedMatrix::operator/= (const double value)");

    return *this;
  }

  double* RowPackedMatrix::DataArray() const
  {
    double* data = new double[_rows * _cols];

    for (int i = 0; i < _rows; i++)
      memcpy(_data + i * _rows, _data[i].DataArray(), _cols);

    return data;
  }

  double RowPackedMatrix::Determinant() const
  {
    // only for (3,3) and (2,2) matrices
    double res = 0.0;
    if ((_rows == 3) && (_cols == 3))
    {
      const RowPackedMatrix& tmp = *this;

      res = tmp(0, 0) * tmp(1, 1) * tmp(2, 2) +
        tmp(0, 1) * tmp(1, 2) * tmp(2, 0) +
        tmp(0, 2) * tmp(1, 0) * tmp(2, 1) -
        tmp(0, 2) * tmp(1, 1) * tmp(2, 0) -
        tmp(0, 0) * tmp(1, 2) * tmp(2, 1) -
        tmp(0, 1) * tmp(1, 0) * tmp(2, 2);
    }
    else if ((_rows == 2) && (_cols == 2))
    {
      const RowPackedMatrix& tmp = *this;
      res = tmp(0, 0) * tmp(1, 1) - tmp(1, 0) * tmp(0, 1);
    }

    return res;
  }

  void RowPackedMatrix::Resize(int rows, int cols)
  {
    if (_data)
      delete[] _data;

    _data = NULL;
    _rows = 0;
    _cols = 0;

    if (rows > 0 && cols > 0)
    {
      _rows = rows;
      _cols = cols;
      _data = new Series[_rows];
    }
  }

  Vector RowPackedMatrix::Row(int row) const
  {
    if ((row >= 0) && (row < _rows))
    {
      Vector result(_cols);
      for (int i = 0; i < _cols; i++)
        result(i) = _data[row](i);

      return result;
    }
    else
      throw std::logic_error("Row is out of range in Vector RowPackedMatrix::Row(int Row)");

    return 0;
  }

  algebra::Series& RowPackedMatrix::RowData(int row)
  {
      return _data[row];
  }

  const algebra::Series& RowPackedMatrix::RowData(int row) const
  {
    return _data[row];
  }

  double RowPackedMatrix::SumRowElements(int row) const
  {
    double result = 0.0;
    if ((row >= 0) && (row < _rows))
    {
      for (int i = 0; i < _cols; i++)
        result += _data[row](i);

      return result;
    }
    else
      throw std::logic_error("Row is out of range in Vector RowPackedMatrix::Row(int Row)");

    return 0;
  }

  Vector RowPackedMatrix::Col(int col) const
  {
    if ((col >= 0) && (col < _cols))
    {
      Vector result(_rows);
      for (int i = 0; i < _rows; i++)
        result(i) = (*this)(i, col);
      return result;
    }
    else
      throw std::logic_error("Col is out of range in Vector RowPackedMatrix::Col(int Col)");

    return 0;
  }

  void RowPackedMatrix::Zero()
  {
    if (_data)
    {
      for (int i = 0; i < _rows; i++)
        _data[i].Zero();
    }
  }

  RowPackedMatrix RowPackedMatrix::operator/(const double value)
  {
    if (value)
    {
      RowPackedMatrix result(*this);
      result /= value;
      return result;
    }
    else
      throw std::logic_error("RowPackedMatrix: Divide By Zero");
  }

  ostream& operator<<(ostream& Stream, const RowPackedMatrix& rhs)
  {

    Stream << "______________________________________________________________________" << endl;

    for (int i = 0; i < rhs.Rows(); i++)
    {
      for (int j = 0; j < rhs.Cols(); j++)
        Stream << rhs(i, j) << " | ";

      Stream << "\n";
    }

    Stream << "______________________________________________________________________" << endl;

    return Stream;
  }

}