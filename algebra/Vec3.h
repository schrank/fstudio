#pragma once

#include <cmath>
#include <iostream>
#include <stdexcept>

namespace algebra
{

  const double ZERO = 1e-18;

  class Vec3
  {
  public:
    Vec3() : _x(0), _y(0), _z(0) {}
    Vec3(double x, double y, double z) : _x(x), _y(y), _z(z) {}
    Vec3(const Vec3& rhs) : _x(rhs._x), _y(rhs._y), _z(rhs._z) {}
    ~Vec3() {}

    Vec3 operator=(Vec3& rhs) {
      return Vec3(_x - rhs._x, _y - rhs._y, _z - rhs._z);
    }

    Vec3 operator+(const Vec3& rhs) const {
      return Vec3(_x + rhs._x, _y + rhs._y, _z + rhs._z);
    }

    Vec3& operator+=(const Vec3& rhs) {
      _x += rhs._x;
      _y += rhs._y;
      _z += rhs._z;
      
      return *this;
    }

    Vec3 operator-(const Vec3& rhs) const {
      return Vec3(_x - rhs._x, _y - rhs._y, _z - rhs._z);
    }

    Vec3 operator/(const double v) const {
      Vec3 result(*this);
      result /= v;
      result /= v;
      result /= v;

      return result;
    }

    Vec3& operator/=(double v) {
      _x /= v;
      _y /= v;
      _z /= v;

      return *this;
    }

    double& operator[](unsigned int index) {
      if (index > 3)
        throw std::logic_error("Vec3::operator[] : index > 3");
      
      return (index == 0) ? _x : (index == 1) ? _y : _z;
    }

    double operator[](unsigned int index) const {
      if (index > 3)
        throw std::logic_error("Vec3::operator[] : index > 3");

      return (index == 0) ? _x : (index == 1) ? _y : _z;
    }

    bool operator==(const Vec3& rhs) {
      if (std::fabs(_x - rhs._x) > ZERO)
        return false;
      if (std::fabs(_y - rhs._y) > ZERO)
        return false;
      if (std::fabs(_z - rhs._z) > ZERO)
        return false;

      return true;
    }

    bool operator<(const Vec3& rhs) const
    {
      if (_x < rhs._x)
        return true;
      else if (_x == rhs._x)
      {
        if (_y < rhs._y)
          return true;
        else if (_y == rhs._y)
        {
          if (_z < rhs._z)
            return true;
        }
      }

      return false;
    }

    double& X() {
      return _x;
    }
    double X() const {
      return _x;
    }
    double& Y() {
      return _y;
    }
    double Y() const {
      return _y;
    }

    double& Z() {
      return _z;
    }
    double Z() const {
      return _z;
    }

    friend std::ostream& operator<<(std::ostream& out, const Vec3& rhs);

  private:
    double _x;
    double _y;
    double _z;
  }; 

}