#include "Matrix.h"

#include <algorithm>
#include <iostream>
#include <fstream>

#include "Vector.h"

using namespace std;

namespace algebra
{

	Matrix::Matrix() : BaseMatrix(), _size(0), _data(nullptr) {}

	Matrix::Matrix(int rows, int cols) : BaseMatrix(rows, cols), _data(nullptr)
	{
		_size = _rows * _cols;

		if (_size > 0)
		{
			_data = new double[_size];

			if (!_data)
				throw std::logic_error("Matrix: No Memory");

			Zero();
		}
	}

	Matrix::Matrix(const Matrix& M) :
		_data(NULL)
	{
		_rows = M.Rows();
		_cols = M.Cols();

		_size = _rows * _cols;

		_data = new double[_size];

		if (!_data)
			throw std::logic_error("Matrix: No Memory");

		for (int i = 0; i < _size; i++)
			_data[i] = M._data[i];
	}

	Matrix::~Matrix()
	{
		if (_data)
			delete[] _data;
	}

	double& Matrix::operator() (int row, int col)
	{
		if ((row >= 0) && (col >= 0) && (row < _rows) && (col < _cols))
			return _data[row + col * _rows];
		else
			throw std::logic_error("Matrix: Out Of Size");
	}

	const double& Matrix::operator() (int row, int col) const
	{
		if ((row >= 0) && (col >= 0) && (row < _rows) && (col < _cols))
			return _data[row + col * _rows];
		else
			throw std::logic_error("Matrix: Out Of Size");
	}

	Matrix& Matrix::operator= (const Matrix& matrix)
	{
		int row = matrix.Rows();
		int col = matrix.Cols();

		if ((row == _rows) && (col == _cols))
			for (int i = 0; i < _size; i++)
				_data[i] = matrix._data[i];

		//		*_data = *matrix._data;

		return *this;
	}

	Matrix& Matrix::operator= (const double value)
	{
		for (int i = 0; i < _size; i++)
			_data[i] = value;

		return *this;
	}

  algebra::Matrix& Matrix::operator*=(const double value) const
  {
		Matrix tmp(*this);

    for (int i = 0; i < _size; i++)
      tmp._data[i] *= value;

    return tmp;
  }

  Matrix& Matrix::operator+= (const Matrix& Matrix)
	{
		int row = Matrix.Rows();
		int col = Matrix.Cols();

		if ((row = _rows) && (col = _cols))
			for (int i = 0; i < _size; i++)
				_data[i] += (Matrix._data)[i];
		else
			throw std::logic_error("Size Mismatch in Matrix& Matrix::operator+= (const Matrix &Matrix)");

		return *this;
	}

	Matrix& Matrix::operator+= (const double value)
	{
		for (int i = 0; i < _size; i++)
			_data[i] += value;

		return *this;
	}

	Matrix& Matrix::operator-= (const Matrix& Matrix)
	{
		int row = Matrix.Rows();
		int col = Matrix.Cols();

		if ((row = _rows) && (col = _cols))
			for (int i = 0; i < _size; i++)
				_data[i] -= (Matrix._data)[i];
		else
			throw std::logic_error("Size Mismatch in Matrix& Matrix::operator-= (const Matrix &Matrix)");

		return *this;
	}

	Matrix& Matrix::operator-= (const double value)
	{
		for (int i = 0; i < _size; i++)
			_data[i] -= value;

		return *this;
	}

	Matrix& Matrix::operator*= (const double value)
	{
		for (int i = 0; i < _size; i++)
			_data[i] *= value;

		return *this;
	}

	Matrix& Matrix::operator/= (const double value)
	{
		if (value)
			for (int i = 0; i < _size; i++)
				_data[i] /= value;
		else
			throw std::logic_error("Zero division in Matrix& Matrix::operator/= (const double value)");

		return *this;
	}

	Matrix& Matrix::operator<=(const Matrix& a)
	{
		int row = min(_rows, a.Rows());
		int col = min(_cols, a.Cols());

		for (int i = 0; i < row; i++)
			for (int j = 0; j < col; j++)
				(*this)(i, j) = a(i, j);

		return *this;
	}

	double* Matrix::DataArray() const
	{
		return _data;
	}

	double Matrix::Determinant() const
	{
		double res = 0.0;
		if ((_rows == 3) && (_cols == 3))
		{
			const Matrix& tmp = *this;

			res = tmp(0, 0) * tmp(1, 1) * tmp(2, 2) +
				tmp(0, 1) * tmp(1, 2) * tmp(2, 0) +
				tmp(0, 2) * tmp(1, 0) * tmp(2, 1) -
				tmp(0, 2) * tmp(1, 1) * tmp(2, 0) -
				tmp(0, 0) * tmp(1, 2) * tmp(2, 1) -
				tmp(0, 1) * tmp(1, 0) * tmp(2, 2);
		}
		else if ((_rows == 2) && (_cols == 2))
		{
			Matrix tmp = *this;
			res = tmp(0, 0) * tmp(1, 1) - tmp(1, 0) * tmp(0, 1);
		}

		return res;
	}

	void Matrix::Resize(int nrows, int ncols)
	{
		_rows = nrows;
		_cols = ncols;
		_size = _rows * _cols;
		if (_data)
		{
			delete[] _data;
			_data = NULL;
		}
		if (_size)
		{
			_data = new double[_size];
			if (!_data)
				throw std::logic_error("Matrix: No Memory");

			Zero();
		}
	}

  Matrix Matrix::Transpose() const
  {
		if (_rows != _cols)
			throw std::logic_error("Matrix::Transpose(): _rows != _cols!");

		Matrix res(_rows, _cols);
		for (int i = 0; i < _rows; i++)
			for (int j = 0; j < i; j++)
			{
				res(i, j) = operator()(j, i);
				res(j, i) = operator()(i, j);
			}

		 return res;
  }

  void Matrix::Enhance(int nrows, int ncols)
	{
		Matrix tmp(*this);
		Resize(_rows + nrows, _cols + ncols);

		*this <= tmp;
	}

	Vector Matrix::Row(int Row) const
	{
		if ((Row < _rows) && (Row >= 0))
		{
			Vector result(_cols);
			for (int i = 0; i < _cols; i++)
				result(i) = (*this)(Row, i);

			return result;
		}
		else
			throw std::logic_error("Row is out of range in Vector Matrix::Row(int Row)");

		return 0;
	}

	double Matrix::SumRowElements(int Row) const
	{
		double result = 0.0;
		if ((Row < _rows) && (Row >= 0))
		{
			for (int i = 0; i < _cols; i++)
				result += (*this)(Row, i);

			return result;
		}
		else
			throw std::logic_error("Row is out of range in Vector Matrix::Row(int Row)");

		return 0;
	}

	Vector Matrix::Col(int Col) const
	{
		if ((Col < _cols) && (Col >= 0))
		{
			Vector result(_rows);
			for (int i = 0; i < _rows; i++)
				result(i) = (*this)(i, Col);
			return result;
		}
		else
			throw std::logic_error("Col is out of range in Vector Matrix::Col(int Col)");

		return 0;
	}

	void Matrix::Zero()
	{
		if (_data)
			memset(_data, 0, sizeof(double) * _size);
	}

	Matrix Matrix::operator+ (const Matrix& a)
	{
		Matrix result(*this);
		result += a;
		return result;
	}

	Matrix Matrix::operator+ (const double value)
	{
		Matrix result(*this);
		result += value;
		return result;
	}

	Matrix Matrix::operator- (const Matrix& a)
	{
		Matrix result(*this);
		result -= a;
		return result;
	}

	Matrix Matrix::operator- (const double value)
	{
		Matrix result(*this);
		result -= value;
		return result;
	}

	Matrix Matrix::operator* (const Matrix& b)
	{
		int brows = b.Rows();
		int bcols = b.Cols();
		double* bp = b._data;
		if (_cols == brows)
		{
			Matrix result(_rows, bcols);
			double* cp = result._data;
			for (int i = 0; i < _rows; i++)
				for (int j = 0; j < bcols; j++)
				{
					double sum = 0;
					for (int k = 0; k < _cols; k++)
						sum += _data[i + k * _rows] * bp[k + j * brows];
					cp[i + j * _rows] = sum;
				}
			return result;
		}	// end if
		else
			throw std::logic_error("Matrix: Sizes Mismatch");
	}

	Vector* Matrix::operator* (const Vector& vector)
	{
		int vsize = vector.Size();
		if (vsize == _cols)
		{
			Vector* result = new Vector(_rows);
			for (int i = 0; i < _rows; i++)
			{
				double sum = 0;
				for (int j = 0; j < _cols; j++)
					sum += vector(j) * _data[i + j * _rows];

				(*result)(i) = sum;
			}	// end for	
			return result;
		}	// end if
		else
			throw std::logic_error("Vector Matrix::operator* (const Vector &vector): Sizes Mismatch");
	}

	Matrix Matrix::operator* (const double value) const
	{
		Matrix result(*this);
		result *= value;
		return result;
	}

	Matrix Matrix::operator/ (const double value)
	{
		if (value)
		{
			Matrix result(*this);
			result /= value;
			return result;
		}
		else
			throw std::logic_error("Matrix: Divide By Zero");
	}

	void Matrix::ZeroRow(int row)
	{
		if (row < _rows)
			for (int i = 0; i < _cols; i++)
				(*this)(row, i) = 0;
	}

	void Matrix::ZeroCol(int col)
	{
		if (col < _cols)
			for (int i = 0; i < _rows; i++)
				(*this)(i, col) = 0;
	}

	void Matrix::AddCol(int col, Vector v)
	{
		if (col < _cols)
			for (int i = 0; i < _rows; i++)
				(*this)(i, col) = v(i);
	}

	void Matrix::AddMatrix(int row, int col, Matrix* A)
	{
		for (int i = 0; i < A->Rows(); i++)
			for (int j = 0; j < A->Cols(); j++)
				(*this)(i + row, j + col) += (*A)(i, j);
	}

	void Matrix::CutMatrix(int row, int col)
	{
		Matrix old(*this);

		this->Resize(row, col);

		for (int i = 0; i < _rows; i++)
			for (int j = 0; j < _cols; j++)
				(*this)(i, j) = old(i, j);
	}

	ostream& operator<<(ostream& Stream, const Matrix& rhs)
	{

		Stream << "______________________________________________________________________" << endl;

		for (int i = 0; i < rhs.Rows(); i++)
		{
			for (int j = 0; j < rhs.Cols(); j++)
				Stream << rhs(i, j) << " | ";

			Stream << "\n";
		}

		Stream << "______________________________________________________________________" << endl;

		return Stream;
	}

}