#include "Vec3.h"

namespace algebra
{

  std::ostream& operator<<(std::ostream& out, const Vec3& rhs)
  {
    out << rhs._x << " " << rhs._y << " " << rhs._z << std::endl;

    return out;
  }

}