#pragma once

namespace algebra
{

	class Series
  {
	public:
		Series();
		Series(Series& series);
		~Series();

		void AddItem(unsigned int col, double value);
		void AddNewItem(unsigned int col, double value);
		int Find(unsigned int col) const;
		void RemoveItem(unsigned int col);
		void RemoveZeros(double nonzero = zero);
		void UpdateItem(unsigned int col, double value);

		void Clear();
		void Sort(unsigned int col);

		unsigned int RealSize() const {
			return _realSize;
		}

		unsigned int Size() const {
			return _size;
		}

		void Zero();

		double* DataArray() const;

		Series& operator= (const Series& series);
		Series& operator+= (const Series& series);
		Series operator+ (const Series& series);
		Series& operator+= (const double a);

		Series& operator-= (const Series& series);
		Series operator- (const Series& series);

		Series& operator*= (const double a);
		double operator* (const Series& a);
		Series operator- ();
	
    double& operator() (int ind);
    const double& operator() (int ind) const;

	private:
		static double zero;

		unsigned int _size;
		unsigned int* _indices;
		double* _values;
		unsigned int _realSize;
  };

}


