#include "Series.h"

#include <iostream>
#include <cstring>
#include <cmath>

#define step 4

namespace algebra
{

	double Series::zero = 1e-16;

	using namespace std;

	Series::~Series(void)
	{
		if (_indices)
			delete[] _indices;
		if (_values)
			delete[] _values;
	}

	Series::Series() :
		_size(0), _indices(NULL), _values(NULL), _realSize(0) {}

	Series::Series(Series& series) :
		_size(0), _indices(NULL), _values(NULL), _realSize(0)
	{
		if (series._size > 0)
		{
			_size = series._size;
			_realSize = _size;
			_values = new double[_size];
			memcpy(_values, series._values, _size * sizeof(double));
			_indices = new unsigned int[_size];
			memcpy(_indices, series._indices, _size * sizeof(unsigned int));
		}
	}

	void Series::AddItem(unsigned int col, double value)
	{
		if (fabs(value) > zero)
		{
			int find = Find(col);
			if (find != -1)
				_values[find] += value;
			else
			{
				AddNewItem(col, value);
				Sort(col);
			}
		}
	}

	void Series::UpdateItem(unsigned int col, double value)
	{
		if (fabs(value) > zero)
		{
			int find = Find(col);
			if (find != -1)
				_values[find] = value;
			else
			{
				AddNewItem(col, value);
				Sort(col);
			}
		}
	}

	void Series::AddNewItem(unsigned int col, double value)
	{
		if (fabs(value) > zero)
		{
			if (_realSize == 0)
			{
				_size = 1;
				_indices = new unsigned int[_size];
				_indices[0] = col;
				_values = new double[_size];
				_values[0] = value;
				_realSize = 1;
			}
			else
			{
				if (_realSize <= _size)
				{
					unsigned int* itmp = _indices;
					_indices = new unsigned int[_size + step];
					memcpy(_indices, itmp, sizeof(unsigned int) * _size);
					delete[] itmp;

					double* vtmp = _values;
					_values = new double[_size + step];
					memcpy(_values, vtmp, sizeof(double) * _size);
					delete[] vtmp;
					_realSize += step;
				}
				_indices[_size] = col;
				_values[_size++] = value;
			}
		}
	}

	void Series::RemoveItem(unsigned int col)
	{
		int find = Find(col);
		if (find != -1)
		{
			for (unsigned int i = find; i < _size - 1; i++)
			{
				_values[i] = _values[i + 1];
				_indices[i] = _indices[i + 1];
			}
			_size--;
			if (!_size)
			{
				delete[] _values;
				_values = NULL;
				delete[] _indices;
				_indices = NULL;
				_realSize = 0;
			}
		}
	}

	int Series::Find(unsigned int col) const
	{
		if (_size)
		{
			if (_indices[_size - 1] > col)
			{
				for (unsigned int i = 0; i < _size; i++)
					if (_indices[i] == col)
						return i;
					else
						if (_indices[i] > col)
							return -1;
			}
			else
				if (_indices[_size - 1] == col)
					return _size - 1;
				else
					return -1;
		}
		else
			return -1;

		return 0;
	}

	void Series::RemoveZeros(double nonzero)
	{
		unsigned size = _size;
		for (unsigned i = 1; i <= size; i++)
		{
			if (fabs(_values[size - i]) < nonzero)
				RemoveItem(_indices[size - i]);
		}
		if (_size < _realSize)
		{
			double* vs = _values;
			_values = new double[_size];
			memcpy(_values, vs, _size * sizeof(double));
			delete[] vs;
			unsigned int* is = _indices;
			_indices = new unsigned int[_size];
			memcpy(_indices, is, _size * sizeof(unsigned int));
			delete[] is;
			_realSize = _size;
		}
	}

	void Series::Clear()
	{
		_size = 0;
		_realSize = 0;

		if (_indices)
			delete[] _indices;
		_indices = NULL;

		if (_values)
			delete[] _values;
		_values = NULL;
	}

	void Series::Sort(unsigned int col)
	{
		for (unsigned int i = 0; i < _size - 1; i++)
			if (_indices[i] > col)
			{
				double value = _values[_size - 1];
				for (unsigned int j = _size - 1; j > i; j--)
				{
					_indices[j] = _indices[j - 1];
					_values[j] = _values[j - 1];
				}
				_indices[i] = col;
				_values[i] = value;
				break;
			}
	}

  void Series::Zero()
  {
		unsigned int size = _size;

		Clear();

		_size = size;
  }

  double* Series::DataArray() const
  {
		double* data = new double[_size];

		for (int i = 0; i < _size; i++)
			data[i] = operator()(i);

		return data;
  }

  Series& Series::operator= (const Series& series)
	{
		if (_indices)
		{
			delete[] _indices;
			_indices = NULL;
		}

		if (_values)
		{
			delete[] _values;
			_values = NULL;
		}

		_size = 0;

		if (series._size > 0)
		{
			_size = series._size;
			_values = new double[_size];
			memcpy(_values, series._values, _size * sizeof(double));
			_indices = new unsigned int[_size];
			memcpy(_indices, series._indices, _size * sizeof(unsigned int));
		}

		_realSize = _size;

		return *this;
	}

  double& Series::operator()(int ind)
  {
		if ((ind >= 0) && (ind < _size))
		{
			if (Find(ind) != -1)
				return _values[ind];
			else
				return zero = 0.0;
		}
    else
      throw std::logic_error("Series: Out Of Size");
  }

  const double& Series::operator()(int ind) const
  {
    if ((ind >= 0) && (ind < _size))
    {
      if (Find(ind) != -1)
        return _values[ind];
      else
        return zero = 0.0;
    }
    else
      throw std::logic_error("Series: Out Of Size");
  }

  Series& Series::operator+= (const Series& series)
	{
		for (unsigned j = 0; j < series._size; j++)
			AddItem(series._indices[j], series._values[j]);

		return *this;
	}

	Series& Series::operator-= (const Series& series)
	{
		for (unsigned j = 0; j < series._size; j++)
			AddItem(series._indices[j], -series._values[j]);

		return *this;
	}

	Series& Series::operator+= (const double a)
	{
		for (unsigned i = 0; i < _size; i++)
			_values[i] += a;

		RemoveZeros();

		return *this;
	}

	Series& Series::operator*= (const double a)
	{
		for (unsigned i = 0; i < _size; i++)
			_values[i] *= a;

		return *this;
	}

	Series Series::operator+ (const Series& series)
	{
		Series res;
		for (unsigned i = 0; i < _size; i++)
			res.AddNewItem(_indices[i], _values[i]);

		for (unsigned j = 0; j < series._size; j++)
			res.AddItem(series._indices[j], series._values[j]);

		return res;
	}

	Series Series::operator- (const Series& series)
	{
		Series res;
		for (unsigned i = 0; i < _size; i++)
			res.AddNewItem(_indices[i], _values[i]);

		for (unsigned j = 0; j < series._size; j++)
			res.AddItem(series._indices[j], -series._values[j]);

		return res;
	}

	Series Series::operator- ()
	{
		Series res;
		for (unsigned i = 0; i < _size; i++)
			res.AddNewItem(_indices[i], -_values[i]);

		return res;
	}

	double Series::operator* (const Series& b)
	{
		double result = 0.0;
		if (_size && b._size)
		{
			unsigned ai = 0;
			unsigned bi = 0;
			do
			{
				if (_indices[ai] == b._indices[bi])
					result += _values[ai++] * b._values[bi++];
				else
					if (_indices[ai] < b._indices[bi])
						ai++;
					else
						bi++;
			} while ((ai < _size) && (bi < b._size));
		}
		return result;
	}

}