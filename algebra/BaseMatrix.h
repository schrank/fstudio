#pragma once

namespace algebra
{

  class BaseMatrix
  {
  public:
    BaseMatrix(): _rows(0), _cols(0) {}
    BaseMatrix(int rows, int cols) : _rows(rows), _cols(cols) {}
    virtual ~BaseMatrix() {}

    int Cols() const { return _cols; }
    int Rows() const { return _rows; }

    virtual double* DataArray() const = 0;

    virtual double& operator()(int row, int col) = 0;
    virtual const double& operator()(int row, int col) const = 0;
    virtual BaseMatrix& operator*=(const double value) const = 0;

    virtual double Determinant() const = 0;
    virtual void Resize(int nrows, int ncols) = 0;

    virtual void ZeroCol(int col) = 0;
    virtual void ZeroRow(int row) = 0;

  protected:
    int _rows;
    int _cols;
  };

}