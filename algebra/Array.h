#pragma once

namespace algebra
{
  template <class T>
  class Array
  {
  public:
    Array(): _dataPtr(nullptr), _arraySize(0) {}

    Array(int size): _dataPtr(0), _arraySize(0)
    {
      if (size > 0)
      {
        _arraySize = size;
        _dataPtr = new T[_arraySize];
        
        if (!_dataPtr)
          throw std::logic_error("algebra::Array - No Memory!");
        
        Zero();
      }
    }

    Array(const Array<T>& array): _dataPtr(0), _arraySize(0)
    {
      if (array.Size() > 0)
      {
        _arraySize = array.Size();
        _dataPtr = new T[_arraySize];
        if (!_dataPtr)
          throw std::logic_error("algebra::Array - No Memory");
        
        for (int i = 0; i < _arraySize; i++)
          _dataPtr[i] = array[i];
      }
    }

    virtual ~Array()
    {
      if (_dataPtr)
        delete[] _dataPtr;
    }

    int Size() const
    {
      return _arraySize;
    }

    T* DataPtr()
    {
      return _dataPtr;
    }

    void Resize(int newsize)
    {
      _arraySize = newsize;
      if (_dataPtr)
      {
        delete[] _dataPtr;
        _dataPtr = NULL;
      }
      if (_arraySize > 0)
      {
        _dataPtr = new T[_arraySize];
        if (!_dataPtr)
          throw std::logic_error("algebra::Array::Resize - No Memory");
        Zero();
      }
      else
      {
        _dataPtr = NULL;
        _arraySize = 0;
      }
    }

    void Zero()
    {
      memset(_dataPtr, '\0', _arraySize * sizeof(T));
    }

    T& operator[](int index)
    {
      if ( index < 0 || index >= _arraySize)
        throw std::logic_error("algebra::Array::operator[] - Out of Size");

      return _dataPtr[index];
    }

    const T operator[](int index) const
    {
      if ((index < 0) || (index >= _arraySize))
        throw std::logic_error("algebra::Array::operator[] - Out of Size");

      return _dataPtr[index];
    }

    operator T*()
    {
      return _dataPtr;
    }

    protected:
      T *_dataPtr;
      int _arraySize;
  };

}