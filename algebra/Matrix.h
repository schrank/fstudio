#pragma once

#include <iostream>

#include "BaseMatrix.h"

namespace algebra
{
	class Vector;

	class Matrix final:
		virtual public BaseMatrix
	{
	public:
		Matrix();
		Matrix(int rows, int cols);
		Matrix(const Matrix& Matrix);
		~Matrix();

		void AddCol(int col, algebra::Vector v);
		void AddMatrix(int row, int col, algebra::Matrix* A);
		void CutMatrix(int row, int col);
		void ZeroCol(int col) override;
		void ZeroRow(int row) override;

		double& operator() (int row, int col) override;
		const double& operator() (int row, int col) const override;

		Matrix& operator= (const Matrix& matrix);
		Matrix& operator= (const double value);
		Matrix& operator+= (const Matrix& Matrix);
		Matrix& operator+= (const double value);
		Matrix& operator-= (const Matrix& Matrix);
		Matrix& operator-= (const double value);
		Matrix& operator*= (const double value);
    Matrix& operator*= (const double value) const override;
		Matrix& operator/= (const double value);
		Matrix& operator<= (const Matrix& a);
		Matrix operator+ (const Matrix& a);
		Matrix operator+ (const double value);
		Matrix operator- (const Matrix& a);
		Matrix operator- (const double value);
		Matrix operator* (const Matrix& b);
		Matrix operator* (const double value) const;
		Vector* operator* (const Vector& vector);
		Matrix operator/ (const double value);

		double* DataArray() const;
		algebra::Vector Col(int Col) const;
		algebra::Vector Row(int Row) const;

		double Determinant() const override;
		void Enhance(int nrows, int ncols);
		void Resize(int nrows, int ncols) override;
		Matrix Transpose() const;
		double SumRowElements(int Row) const;
		void Zero();

	protected:
		friend std::ostream& operator<<(std::ostream& Stream, const algebra::Matrix& rhs);

	protected:
		long _size;

		static double zero;

		double* _data;
	};

	std::ostream& operator<<(std::ostream& Stream, const algebra::Matrix& rhs);

}

