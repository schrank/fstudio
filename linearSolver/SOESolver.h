#pragma once

#include "../algebra/Matrix.h"
#include "../algebra/Vector.h"

namespace ls
{

  class SOESolver
  {
  public:
    SOESolver() {}
    virtual ~SOESolver() {}

    virtual algebra::Vector Solve(algebra::Matrix* A, algebra::Vector* b) = 0;
  };

}