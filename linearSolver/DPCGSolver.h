#pragma once

#include "SOESolver.h"

namespace ls
{
  class DPCGSolver : virtual public SOESolver
  {
  public:
    DPCGSolver();
    DPCGSolver(double precision);
    virtual ~DPCGSolver();

    algebra::Vector Solve(algebra::Matrix* A, algebra::Vector* b) override;

  private:
    double _precision;
  };
}