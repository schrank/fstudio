#include "DPCGSolver.h"

#include <iostream>

namespace ls
{
  DPCGSolver::DPCGSolver(): _precision(1e-8) {}

	DPCGSolver::DPCGSolver(double precision): _precision(precision)	{}

  DPCGSolver::~DPCGSolver() {}

  algebra::Vector DPCGSolver::Solve(algebra::Matrix* A, algebra::Vector* b)
  {
		// number of equations
		unsigned int noe = b->Size();

		algebra::Vector r(*b);
		algebra::Vector p(noe);
		algebra::Vector x(noe);
		algebra::Vector z(noe);
		algebra::Vector M(noe);

		for (unsigned int i = 0; i < noe; i++)
			if ((*A)(i, i) != 0.0)
				M(i) = 1.0 / (*A)(i, i);
			else
				M(i) = 1.0;

		for (unsigned int i = 0; i < noe; i++)
			z(i) = M(i) * r(i);

		p = z;

		double rho = r * z;

		double error = r.Norm();

		unsigned int step = 1;

		double r0;

		algebra::Vector Ap(noe);

		while (error > _precision && step < 10 * noe)
		{
			Ap = *(*A * p);
			double alpha = (r * z) / (p * Ap);

			x += p * alpha;

			r -= Ap * alpha;

			for (unsigned int i = 0; i < noe; i++)
				z(i) = M(i) * r(i);

			double oldrho = rho;

			rho = z * r;

			double beta = rho / oldrho;

			p = z + p * beta;

			if (step == 1)
				r0 = r.Norm();

			if (fabs(r0) < _precision)
				break;

			error = r.Norm() / r0;

			step++;
		}

		return x;
  }

}